use std::io::{BufReader, BufRead};

struct InputEntry {
    text: String,
}

struct ParsedInput {
    entries: Vec<InputEntry>,
}

fn parse_entry<T: AsRef<str>>(line: T) -> InputEntry {
    InputEntry {
        text: line.as_ref().to_string(),
    }
}

fn parse_input(file: &str) -> std::io::Result<ParsedInput> {
    let file = std::fs::File::open(format!("{}/{}", env!("CARGO_PKG_NAME"), file))?;
    let reader = BufReader::new(file);
    let entries = reader.lines()
        .map(Result::unwrap)
        .map(parse_entry)
        .collect();

    Ok(ParsedInput {
        entries,
    })
}

fn part1(input: &ParsedInput) -> usize {
    0
}

fn part2(input: &ParsedInput) -> usize {
    0
}

fn main() {
    let test_input = parse_input("test.txt").unwrap();
    assert_eq!(part1(&test_input), 0);
    
    let parsed_input = parse_input("input.txt").unwrap();
    println!("{}", part1(&parsed_input));

    assert_eq!(part2(&test_input), 0);
    println!("{}", part2(&parsed_input));
}

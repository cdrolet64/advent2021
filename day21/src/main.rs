use std::{io::{BufReader, BufRead}, collections::HashMap};

struct ParsedInput {
    starting_positions: [u8; 2],
}

fn parse_line<T: AsRef<str>>(line: T) -> u8 {
    line.as_ref()
        .split(": ")
        .skip(1)
        .next()
        .unwrap()
        .parse()
        .unwrap()
}

fn parse_input(file: &str) -> std::io::Result<ParsedInput> {
    let file = std::fs::File::open(format!("{}/{}", env!("CARGO_PKG_NAME"), file))?;
    let reader = BufReader::new(file);
    let entries: Vec<_> = reader.lines()
        .map(Result::unwrap)
        .map(parse_line)
        .collect();

    Ok(ParsedInput {
        starting_positions: [entries[0], entries[1]],
    })
}

fn roll_dices(turn: usize) -> usize {
    3 * (3 * turn + 2)
}

fn rolled_count(turn: usize) -> usize {
    3 * turn
}

fn advance_position(start_position: u8, dices: usize) -> u8 {
    (((dices + start_position as usize) - 1) % 10 + 1) as u8
}

fn part1(input: &ParsedInput) -> usize {
    let mut turn = 0;
    let mut scores = [0usize; 2];
    let mut positions = input.starting_positions.clone();

    loop {
        let dices = roll_dices(turn);

        positions[turn % 2] = advance_position(positions[turn % 2], dices);
        scores[turn % 2] += positions[turn % 2] as usize;

        if scores[turn % 2] >= 1000 {
            turn += 1;
            return rolled_count(turn) * scores[turn % 2];
        }

        turn += 1;
    }
}

fn multiply_wins(wins: &[usize; 2], count: usize) -> [usize; 2] {
    [wins[0] * count, wins[1] * count]
}

fn add_wins(wins1: &[usize; 2], wins2: &[usize; 2]) -> [usize; 2] {
    [wins1[0] + wins2[0], wins1[1] + wins2[1]]
}

const OUTCOMES: &[(usize, usize)] = &[
    (3usize, 1usize), // 1-1-1
    (4usize, 3usize), // 1-1-2, 1-2-1, 2-1-1
    (5usize, 6usize), // 1-1-3, 1-3-1, 3-1-1, 2-2-1, 2-1-2, 1-2-2
    (6usize, 7usize), // 2-2-2, 3-2-1, 3-1-2, 1-2-3, 1-3-2, 2-3-1, 2-1-3
    (7usize, 6usize),
    (8usize, 3usize),
    (9usize, 1usize),
];

struct OutcomeCache {
    outcomes: HashMap<(usize, [usize; 2], [u8; 2]), [usize; 2]>,
}

impl OutcomeCache {
    fn new() -> Self {
        Self {
            outcomes: HashMap::new(),
        }
    }

    fn get(&self, turn: usize, scores: &[usize; 2], positions: &[u8; 2]) -> Option<[usize; 2]> {
        self.outcomes.get(&(turn % 2, scores.clone(), positions.clone())).cloned()
    }

    fn add(&mut self, turn: usize, scores: &[usize; 2], positions: &[u8; 2], outcome: [usize; 2]) {
        self.outcomes.insert((turn % 2, scores.clone(), positions.clone()), outcome);
    }
}

fn compute_wins(cache: &mut OutcomeCache, turn: usize, scores: &[usize; 2], positions: &[u8; 2]) -> [usize; 2] {
    if scores[0] >= 21 {
        return [1, 0];
    }
    if scores[1] >= 21 {
        return [0, 1];
    }

    if let Some(wins) = cache.get(turn, scores, positions) {
        return wins;
    }

    let next_turn = turn + 1;

    let wins = OUTCOMES.iter()
        .map(|(dices, count)| {
            let mut positions = positions.clone();
            positions[turn % 2] = advance_position(positions[turn % 2], *dices);
            let mut scores = scores.clone();
            scores[turn % 2] += positions[turn % 2] as usize;
            multiply_wins(&compute_wins(cache, next_turn, &scores, &positions), *count)
        })
        .reduce(|wins, new_wins| add_wins(&wins, &new_wins))
        .unwrap();

    cache.add(turn, scores, positions, wins.clone());
    wins
}

fn part2(input: &ParsedInput) -> usize {
    let wins = compute_wins(&mut OutcomeCache::new(), 0, &[0usize; 2], &input.starting_positions);

    *wins.iter().max().unwrap()
}

fn main() {
    let test_input = parse_input("test.txt").unwrap();
    assert_eq!(part1(&test_input), 739785);
    
    let parsed_input = parse_input("input.txt").unwrap();
    println!("{}", part1(&parsed_input));

    assert_eq!(part2(&test_input), 444356092776315);
    println!("{}", part2(&parsed_input));
}


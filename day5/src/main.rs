use std::str::FromStr;
use std::io::{BufReader, BufRead};

#[derive(Clone, PartialEq, Eq)]
struct Point {
    x: i32,
    y: i32,
}

impl Point {
    fn from<T: Iterator<Item = i32>>(iter: &mut T) -> Point {
        Point {
            x: iter.next().unwrap(),
            y: iter.next().unwrap(),
        }
    }

    fn translate(&self, delta: &Point) -> Point {
        Point {
            x: self.x + delta.x,
            y: self.y + delta.y,
        }
    }

    fn opposite(&self) -> Point {
        Point {
            x: -self.x,
            y: -self.y,
        }
    }
}

struct Segment {
    from: Point,
    to: Point,
}

impl Segment {
    fn from<T: Iterator<Item = Point>>(iter: &mut T) -> Segment {
        Segment {
            from: iter.next().unwrap(),
            to: iter.next().unwrap(),
        }
    }
}

struct ParsedInput {
    segments: Vec<Segment>,
}

fn parse_input() -> std::io::Result<ParsedInput> {
    let file = std::fs::File::open("day5/input.txt")?;
    let reader = BufReader::new(file);
    let segments = reader.lines()
        .map(|line| Segment::from(&mut line.unwrap()
            .split(" -> ")
            .map(|ps| {
                let mut iter = ps.split(',').map(|v| i32::from_str(v).unwrap());
                Point::from(&mut iter)
            })))
        .collect();
        

    Ok(ParsedInput {
        segments,
    })
}

struct Field {
    grid: Vec<usize>,
}

impl Field {
    fn new() -> Field {
        Field {
            grid: Vec::from_iter(std::iter::repeat(0).take(1000 * 1000)),
        }
    }

    fn cover(&mut self, p: &Point) {
        assert!(p.x > 0 && p.x < 1000);
        assert!(p.y > 0 && p.y < 1000);
        self.grid[usize::try_from(p.y).unwrap() * 1000 + usize::try_from(p.x).unwrap()] += 1;
    }

    fn overlapping_count(&self) -> usize {
        self.grid.iter()
            .filter(|&&c| c > 1)
            .count()
    }
}

fn is_parallel_to_axis(segment: &Segment) -> bool {
    segment.from.x == segment.to.x || segment.from.y == segment.to.y
}

fn is_diagonal(p: &Point) -> bool {
    p.x.abs() == p.y.abs()
}

fn direction(from: i32, to: i32) -> i32 {
    match from.cmp(&to) {
        std::cmp::Ordering::Equal => 0,
        std::cmp::Ordering::Less => 1,
        std::cmp::Ordering::Greater => -1,
    }
}

fn cover_segment(mut field: Field, segment: &Segment) -> Field {
    let delta = Point {
        x: direction(segment.from.x, segment.to.x),
        y: direction(segment.from.y, segment.to.y),
    };
    let mut p = segment.from.clone();
    while p != segment.to {
        field.cover(&p);
        p = p.translate(&delta);
    }
    field.cover(&p);
    field
}

fn count_overlapping<F: Fn(&Segment) -> bool>(input: &ParsedInput, f: F) -> usize {
    let field = Field::new();
    let field = input.segments.iter()
        .filter(|&segment| f(segment))
        .fold(field, cover_segment);

    field.overlapping_count()
}

fn part1(input: &ParsedInput) -> usize {
    count_overlapping(input, is_parallel_to_axis)
}

fn part2(input: &ParsedInput) -> usize {
    count_overlapping(input, |segment| is_parallel_to_axis(segment) || is_diagonal(&segment.to.opposite().translate(&segment.from)))
}

fn main() {
    let parsed_input = parse_input().unwrap();
    println!("{}", part1(&parsed_input));
    println!("{}", part2(&parsed_input));
}

use std::str::FromStr;
use std::io::{BufRead, BufReader};
use std::fs::File;

fn parse_file() -> std::io::Result<Vec<(String, u32)>> {
    let file = File::open("day2/input.txt")?;
    let reader = BufReader::new(file);
    reader.lines().map(|line|
        line.map(|line| {
            let mut itr = line.split(' ');
            let direction = String::from(itr.next().unwrap());
            let distance = u32::from_str(&itr.next().unwrap()).unwrap();
            (direction, distance)
        })).collect()
}

fn part1(values: &[(String, u32)]) {
    let mut depth = 0;
    let mut horizontal = 0;

    for (direction, distance) in values.iter() {
        if direction == "forward" {
            horizontal += distance;
        }
        else if direction == "down" {
            depth += distance;
        }
        else if direction == "up" {
            depth -= distance;
        }
        else {
            panic!("Wrong direction!");
        }
    }

    println!("{}", depth * horizontal);
}

fn part2(values: &[(String, u32)]) {
    let mut depth = 0;
    let mut horizontal = 0;
    let mut aim = 0;

    for (direction, distance) in values.iter() {
        if direction == "forward" {
            horizontal += distance;
            depth += aim * distance;
        }
        else if direction == "down" {
            aim += distance;
        }
        else if direction == "up" {
            aim -= distance;
        }
        else {
            panic!("Wrong direction!");
        }
    }

    println!("{}", depth * horizontal);
}

fn main() {
    let values = parse_file().unwrap();
    part1(&values);
    part2(&values);
}

use std::{io::{BufReader, BufRead}, collections::HashSet};

#[derive(Clone, PartialEq, Eq, Hash)]
struct Point {
    x: i32,
    y: i32,
}

impl Point {
    const fn new(x: i32, y: i32) -> Point {
        Point { x, y }
    }
}

enum Axis {
    X,
    Y,
}

fn fold(pivot: i32, value: i32) -> i32 {
    if value > pivot {
        2 * pivot - value
    } else {
        value
    }
}

struct Instruction {
    axis: Axis,
    value: i32,
}

impl Instruction {
    fn execute(&self, p: &Point) -> Point {
        match self.axis {
            Axis::X => Point::new(fold(self.value, p.x), p.y),
            Axis::Y => Point::new(p.x, fold(self.value, p.y)),
        }
    }
}

struct ParsedInput {
    dots: HashSet<Point>,
    instructions: Vec<Instruction>,
}

fn parse_entry<T: AsRef<str>>(line: T) -> Point {
    let mut it = line.as_ref().split(',');

    Point::new(
        it.next().unwrap().parse().unwrap(),
        it.next().unwrap().parse().unwrap())
}

fn parse_instruction<T: AsRef<str>>(line: T) -> Instruction {
    let mut it = line.as_ref().split(' ');
    it.next().unwrap();
    it.next().unwrap();
    let mut it = it.next().unwrap().split('=');

    Instruction {
        axis: match it.next().unwrap() {
            "x" => Axis::X,
            "y" => Axis::Y,
            a => unreachable!("Unexpected axis: {}", a),
        },
        value: it.next().unwrap().parse().unwrap(),
    }
}

fn parse_input(file: &str) -> std::io::Result<ParsedInput> {
    let file = std::fs::File::open(format!("{}/{}", env!("CARGO_PKG_NAME"), file))?;
    let reader = BufReader::new(file);

    let mut it_lines = reader.lines()
        .map(Result::unwrap);
    let dots = (&mut it_lines)
        .take_while(|line| !line.is_empty())
        .map(parse_entry)
        .collect();

    let instructions = it_lines.map(|line| parse_instruction(line))
        .collect();

    Ok(ParsedInput {
        dots,
        instructions,
    })
}

fn part1(input: &ParsedInput) -> usize {
    let dots = input.dots.clone();
    let dots: HashSet<_> = dots.into_iter().map(|dot| input.instructions[0].execute(&dot)).collect();
    dots.len()
}

fn part2(input: &ParsedInput) -> String {
    let mut dots = input.dots.clone();
    for instruction in input.instructions.iter() {
        dots = dots.into_iter()
            .map(|dot| instruction.execute(&dot))
            .collect();
    }

    let width = dots.iter().map(|p| p.x).max().unwrap() as usize + 1;
    let height = dots.iter().map(|p| p.y).max().unwrap() as usize + 1;

    let mut image: Vec<Vec<_>> = std::iter::repeat(
        std::iter::repeat(' ')
            .take(width)
            .collect())
        .take(height)
        .collect();

    for p in dots.iter() {
        image[p.y as usize][p.x as usize] = '#';
    }

    image.into_iter()
        .map(|s| s.into_iter().collect::<String>())
        .reduce(|s, v| s + "\n" + &v)
        .unwrap()
}

fn main() {
    let test_input = parse_input("test.txt").unwrap();
    assert_eq!(part1(&test_input), 17);
    
    let parsed_input = parse_input("input.txt").unwrap();
    println!("{}", part1(&parsed_input));

    assert_eq!(part2(&test_input), "#####\n#   #\n#   #\n#   #\n#####");
    println!("{}", part2(&parsed_input));
}

use std::{io::{BufReader, BufRead}, collections::HashMap};

#[derive(PartialEq, Eq, Clone, Copy, Hash)]
enum AmphipodKind {
    Amber = 0,
    Bronze = 1,
    Copper = 2,
    Desert = 3,
}

impl PartialOrd for AmphipodKind {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        (*self as u32).partial_cmp(&(*other as u32))
    }
}

impl Ord for AmphipodKind {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        (*self as u32).cmp(&(*other as u32))
    }
}

impl AmphipodKind {
    fn from_u8(v: u8) -> Self {
        match v {
            0 => Self::Amber,
            1 => Self::Bronze,
            2 => Self::Copper,
            3 => Self::Desert,
            _ => unreachable!(),
        }
    }

    fn from_char(c: char) -> Self {
        match c {
            'A' => Self::Amber,
            'B' => Self::Bronze,
            'C' => Self::Copper,
            'D' => Self::Desert,
            _ => unreachable!(),
        }
    }

    fn movement_cost(&self) -> usize {
        match self {
            Self::Amber => 1,
            Self::Bronze => 10,
            Self::Copper => 100,
            Self::Desert => 1000,
        }
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Hash)]
enum HallWayKind {
    Entrance,
    InBetween,
    Extremity(usize),
}

impl HallWayKind {
    fn can_hold_amphipod(&self) -> bool {
        match self {
            Self::Entrance => false,
            Self::InBetween => true,
            Self::Extremity(_) => true,
        }
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Hash)]
struct HallWay {
    hallway_kind: HallWayKind,
    room_kind: AmphipodKind,
}

impl HallWay {
    const fn new(hallway_kind: HallWayKind, room_kind: AmphipodKind) -> Self {
        Self {
            hallway_kind,
            room_kind,
        }
    }
}

const HALLWAYS: &[HallWay] = &[
    HallWay::new(HallWayKind::Extremity(1), AmphipodKind::Amber),
    HallWay::new(HallWayKind::Extremity(0), AmphipodKind::Amber),
    HallWay::new(HallWayKind::Entrance, AmphipodKind::Amber),
    HallWay::new(HallWayKind::InBetween, AmphipodKind::Amber),
    HallWay::new(HallWayKind::Entrance, AmphipodKind::Bronze),
    HallWay::new(HallWayKind::InBetween, AmphipodKind::Bronze),
    HallWay::new(HallWayKind::Entrance, AmphipodKind::Copper),
    HallWay::new(HallWayKind::InBetween, AmphipodKind::Copper),
    HallWay::new(HallWayKind::Entrance, AmphipodKind::Desert),
    HallWay::new(HallWayKind::Extremity(0), AmphipodKind::Desert),
    HallWay::new(HallWayKind::Extremity(1), AmphipodKind::Desert),
];

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Hash)]
struct Room {
    kind: AmphipodKind,
    size: usize,
}

#[derive(PartialEq, Eq, Clone, Hash)]
struct MapState {
    hallways: Vec<Option<AmphipodKind>>,
    rooms: Vec<Vec<AmphipodKind>>,
}

impl MapState {
    fn start_from_iter<I: Iterator<Item = AmphipodKind>, J: Iterator<Item = I>>(it: J) -> Self {
        Self {
            hallways: vec![None; HALLWAYS.len()],
            rooms: it.map(|it_stack| it_stack.collect())
                .collect(),
        }
    }

    fn is_final(&self) -> bool {
        self.hallways.iter()
            .all(|hallway| hallway.is_none()) &&
            self.rooms.iter()
                .enumerate()
                .all(|(room, amphipods)| {
                    let room_kind = AmphipodKind::from_u8(room as u8);
                    amphipods.iter()
                        .all(|amphipod| amphipod == &room_kind)
                })
    }

    fn room_size(&self) -> usize {
        let total_amphipods = self.hallways.iter()
            .filter(|hallway| hallway.is_some())
            .count() +
            self.rooms.iter()
                .map(|room| room.len())
                .sum::<usize>();

        assert!(total_amphipods == 8 || total_amphipods == 16);

        total_amphipods / self.rooms.len()
    }

    fn room_is_good(&self, room_kind: &AmphipodKind) -> bool {
        self.rooms[*room_kind as u8 as usize]
            .iter()
            .all(|amphipod| amphipod == room_kind)
    }

    fn path_is_clear(&self, from: usize, to: usize) -> bool {
        let range = make_range(from, to);

        !self.hallways[range.0..=range.1].iter()
            .any(|hold| hold.is_some())
    }

    fn moves_to_enter(&self, room_kind: &AmphipodKind) -> usize {
        self.room_size() - self.rooms[*room_kind as u8 as usize].len()
    }

    fn moves_to_exit(&self, room_kind: &AmphipodKind) -> usize {
        self.moves_to_enter(room_kind) + 1
    }

    fn moves_to_hallway(&self, from: usize, to: usize) -> usize {
        let range = make_range(from, to);
        range.1 + 1 - range.0
    }

    fn enter_room(&self, from: usize, kind: &AmphipodKind) -> Option<(MapState, usize)> {
        let to = find_entrance_for(kind);

        if !self.room_is_good(kind) {
            return None;
        }

        if !self.path_is_clear(from, to) {
            return None;
        }

        let new_state = MapState {
            hallways: self.hallways.iter()
                .enumerate()
                .map(|(i, kind)| if i == from { None } else { kind.clone() })
                .collect(),
            rooms: self.rooms.iter()
                .enumerate()
                .map(|(room_kind, room)| {
                    let mut new_room = room.clone();
                    if room_kind == *kind as u8 as usize {
                        new_room.push(kind.clone());
                    }
                    new_room
                })
                .collect(),
        };

        let cost = (self.moves_to_enter(kind) + self.moves_to_hallway(from, to)) * kind.movement_cost();

        Some((new_state, cost))
    }

    fn iter_hallway_to_room<'a>(&'a self) -> impl Iterator<Item = (MapState, usize)> + 'a {
        self.hallways.iter()
            .enumerate()
            .filter_map(|(from, kind)|
                kind.and_then(|kind| self.enter_room(from, &kind)))
    }

    fn iter_room_to_hallway<'a>(&'a self) -> impl Iterator<Item = (MapState, usize)> + 'a {
        self.rooms.iter()
            .enumerate()
            .map(|(room_kind, room)| (AmphipodKind::from_u8(room_kind as u8), room))
            .filter(|(room_kind, _room)| !self.room_is_good(room_kind))
            .map(move |(room_kind, room)| {
                let from = find_entrance_for(&room_kind);
                let moving_kind = room.last().unwrap();

                HALLWAYS.iter()
                    .enumerate()
                    .filter(move |(to, hallway)| hallway.hallway_kind.can_hold_amphipod() && self.hallways[*to as u8 as usize].is_none() && self.path_is_clear(from, *to))
                    .map(move |(to, _hallway)| {
                        let new_state = Self {
                            hallways: self.hallways.iter()
                                .enumerate()
                                .map(|(i, kind)| if i == to { Some(moving_kind.clone()) } else { kind.clone() })
                                .collect(),
                            rooms: self.rooms.iter()
                                .enumerate()
                                .map(|(new_room_kind, room)| {
                                    let mut new_room = room.clone();
                                    if AmphipodKind::from_u8(new_room_kind as u8) == room_kind {
                                        new_room.pop();
                                    }
                                    new_room
                                })
                                .collect(),
                        };

                        let move_cost = (self.moves_to_exit(&room_kind) + self.moves_to_hallway(from, to)) * moving_kind.movement_cost();

                        (new_state, move_cost)
                    })
            })
            .flatten()
    }
}

struct ParsedInput {
    rooms: Vec<Vec<AmphipodKind>>,
}

fn read_line(str: &str) -> Vec<AmphipodKind> {
    str.chars()
        .skip(3)
        .step_by(2)
        .take_while(|&c| c >= 'A' && c <= 'D')
        .map(AmphipodKind::from_char)
        .collect()
}

fn parse_input(file: &str) -> std::io::Result<ParsedInput> {
    let file = std::fs::File::open(format!("{}/{}", env!("CARGO_PKG_NAME"), file))?;
    let reader = BufReader::new(file);
    let mut it_lines = reader.lines()
        .map(Result::unwrap)
        .skip(2);

    let tops = read_line(&it_lines.next().unwrap());
    let bottoms = read_line(&it_lines.next().unwrap());

    Ok(ParsedInput {
        rooms: tops.into_iter()
            .zip(bottoms.into_iter())
            .map(|(top, bottom)| vec![bottom, top])
            .collect(),
    })
}

fn make_range(from: usize, to: usize) -> (usize, usize) {
    if from > to {
        (to, from - 1)
    } else {
        (from + 1, to)
    }
}

fn find_entrance_for(kind: &AmphipodKind) -> usize {
    (((*kind as u8) + 1) * 2) as usize
}

fn solve(state: &MapState, cache: &mut HashMap<MapState, Option<usize>>) -> Option<usize> {
    if state.is_final() {
        return Some(0);
    }

    if let Some(value) = cache.get(state) {
        return *value;
    }

    let mut min = None;
    
    for (next_state, cost) in state.iter_hallway_to_room()
        .chain(state.iter_room_to_hallway()) {
        if let Some(v_min) = min {
            if v_min <= cost {
                continue;
            }
        }
        
        let other_min = match solve(&next_state, cache) {
            Some(solve_cost) => solve_cost + cost,
            None => continue,
        };

        if min.is_none() || other_min < min.unwrap() {
            min = Some(other_min);
        }
    }

    cache.insert(state.clone(), min);

    min
}

fn part1(input: &ParsedInput) -> usize {
    let initial_state = MapState::start_from_iter(input.rooms.iter()
        .map(|v| v.iter().cloned()));

    assert!(initial_state.path_is_clear(0, 10));
    assert!(!initial_state.room_is_good(&AmphipodKind::Amber));
    assert!(!initial_state.room_is_good(&AmphipodKind::Bronze));
    assert!(!initial_state.room_is_good(&AmphipodKind::Copper));
    assert!(!initial_state.room_is_good(&AmphipodKind::Desert));

    solve(&initial_state, &mut HashMap::new()).unwrap()
}

fn part2(input: &ParsedInput) -> usize {
    const ADDED_AMPHIPODS: [[AmphipodKind; 2]; 4] = [
        [AmphipodKind::Desert, AmphipodKind::Desert],
        [AmphipodKind::Bronze, AmphipodKind::Copper],
        [AmphipodKind::Amber, AmphipodKind::Bronze],
        [AmphipodKind::Copper, AmphipodKind::Amber],
    ];

    let initial_state = MapState::start_from_iter(input.rooms.iter()
        .zip(ADDED_AMPHIPODS.iter())
        .map(|(stack, to_insert)| stack.iter()
            .take(1)
            .chain(to_insert)
            .chain(stack.iter()
                .skip(1))
            .cloned()));
    
    solve(&initial_state, &mut HashMap::new()).unwrap()
}

fn main() {
    let test_input = parse_input("test.txt").unwrap();
    assert_eq!(part1(&test_input), 12521);
    
    let parsed_input = parse_input("input.txt").unwrap();
    println!("{}", part1(&parsed_input));

    assert_eq!(part2(&test_input), 44169);
    println!("{}", part2(&parsed_input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn solve_test() {
        {
            let initial_state = MapState {
                hallways: vec![None, Some(AmphipodKind::Amber), None, None, None, None, None, None, None, None, None],
                rooms: vec![vec![AmphipodKind::Amber], vec![AmphipodKind::Bronze; 2], vec![AmphipodKind::Copper; 2], vec![AmphipodKind::Desert; 2]],
            };

            assert_eq!(solve(&initial_state, &mut HashMap::new()), Some(2));
        }
        {
            let initial_state = MapState {
                hallways: vec![None, Some(AmphipodKind::Amber), None, None, None, None, None, None, None, None, None],
                rooms: vec![vec![AmphipodKind::Amber, AmphipodKind::Bronze], vec![AmphipodKind::Bronze], vec![AmphipodKind::Copper; 2], vec![AmphipodKind::Desert; 2]],
            };

            assert_eq!(solve(&initial_state, &mut HashMap::new()), Some(42));
        }
    }
}

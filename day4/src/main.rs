use std::str::FromStr;
use std::io::{BufReader, BufRead};

#[derive(Debug,Clone)]
struct BingoNumber {
    number: u32,
    marked: bool,
}

impl BingoNumber {
    fn from(number: u32) -> Self {
        Self {
            number,
            marked: false,
        }
    }
}

#[derive(Debug,Clone)]
struct Board {
    grid: [[BingoNumber; 5]; 5],
}

impl Board {
    fn parse<T: BufRead>(lines: &mut std::io::Lines<T>) -> Board {
        Self {
            grid: lines.take(5)
                .map(|l| l.unwrap())
                .map(|l| l.split(' ')
                    .filter(|e| !e.is_empty())
                    .map(|n| BingoNumber::from(u32::from_str(n).unwrap()))
                    .collect::<Vec<_>>()
                    .try_into()
                    .unwrap())
                .collect::<Vec<_>>()
                .try_into()
                .unwrap()
        }
    }

    fn mark(&mut self, number: u32) {
        self.grid.iter_mut()
            .for_each(|l|
                l.iter_mut()
                    .for_each(|c| if c.number == number { c.marked = true; }))
    }

    fn is_complete(&self) -> bool {
        self.grid.iter().any(|row| row.iter().all(|c| c.marked)) ||
            (0..5).any(|i| self.grid.iter().all(|row| row[i].marked))
    }

    fn unmarked_sum(&self) -> u32 {
        self.grid.iter()
            .map(|row|
                row.iter()
                    .filter(|c| !c.marked)
                    .map(|c| c.number)
                    .sum::<u32>())
            .sum()
    }
}

fn parse_file() -> std::io::Result<(Vec<u32>, Vec<Board>)> {
    let file = std::fs::File::open("day4/input.txt")?;
    let reader = BufReader::new(file);
    let mut lines = reader.lines();
    let numbers_line = lines.next().unwrap()?;
    let numbers: Vec<_> = numbers_line.split(',').map(|n| u32::from_str(n).unwrap()).collect();
    let mut boards = Vec::new();

    while lines.next().is_some() {
        boards.push(Board::parse(&mut lines));
    }

    Ok((numbers, boards))
}

fn part1(numbers: &[u32], mut boards: Vec<Board>) -> u32 {
    for number in numbers {
        boards.iter_mut().for_each(|b| b.mark(*number));

        let winner = match boards.iter().find(|b| b.is_complete()) {
            None => continue,
            Some(winner ) => winner,
        };

        return winner.unmarked_sum() * number;
    }

    unreachable!();
}

fn part2(numbers: &[u32], mut boards: Vec<Board>) -> u32 {
    for number in numbers {
        boards.iter_mut().for_each(|b| b.mark(*number));

        if boards.len() == 1 && boards.first().unwrap().is_complete() {
            return boards.first().unwrap().unmarked_sum() * number;
        }

        boards = boards.into_iter().filter(|b| !b.is_complete()).collect();
    }

    unreachable!();
}

fn main() {
    let (numbers, boards) = parse_file().unwrap();
    println!("{}", part1(&numbers, boards.clone()));
    println!("{}", part2(&numbers, boards.clone()));
}

use std::{io::{BufReader, BufRead}, fmt::Display};

#[derive(PartialEq, Eq, Clone)]
enum Element {
    Regular(u32),
    Snailfish(Box<SnailfishNumber>),
}

impl Element {
    fn magnitude(&self) -> usize {
        match self {
            Self::Regular(v) => *v as usize,
            Self::Snailfish(s) => s.magnitude(),
        }
    }

    fn explodes(&self, nested: usize) -> Option<(u32, u32)> {
        match self {
            Element::Regular(_) => None,
            Element::Snailfish(s) => s.explodes(nested),
        }
    }

    fn explode_subpair(&mut self, nested: usize) -> Option<(u32, u32)> {
        match self {
            Element::Regular(_) => None,
            Element::Snailfish(s) => s.explode_subpair(nested),
        }
    }

    fn add_to(&mut self, index: usize, v: u32) {
        if v == 0 {
            return;
        }

        match self {
            Element::Regular(n) => *n += v,
            Element::Snailfish(s) => s.add_to(index, v),
        }
    }

    fn split(&mut self) -> bool {
        match self {
            Element::Regular(n) => {
                if *n < 10 {
                    return false;
                }

                let half = *n / 2;
                let left = Element::Regular(half);
                let right = Element::Regular(*n - half);
                let splitted = SnailfishNumber { elements: [left, right] };
                *self = Element::Snailfish(Box::new(splitted));
                true
            },
            Element::Snailfish(s) => s.split(),
        }
    }
}

impl Display for Element {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Element::Regular(v) => v.fmt(f),
            Element::Snailfish(s) => s.fmt(f),
        }
    }
}

#[derive(PartialEq, Eq, Clone)]
struct SnailfishNumber {
    elements: [Element; 2],
}

impl SnailfishNumber {
    fn magnitude(&self) -> usize {
        3 * self.elements[0].magnitude() + 2 * self.elements[1].magnitude()
    }

    fn explodes(&self, nested: usize) -> Option<(u32, u32)> {
        if nested < 4 {
            return None;
        }

        match self.elements {
            [Element::Regular(first), Element::Regular(second)] => Some((first, second)),
            _ => None,
        }
    }

    fn explode_subpair(&mut self, nested: usize) -> Option<(u32, u32)> {
        if let Some((left, right)) = self.elements[0].explodes(nested + 1) {
            self.elements[0] = Element::Regular(0);
            self.elements[1].add_to(0, right);
            return Some((left, 0));
        }

        if let Some((left, right)) = self.elements[0].explode_subpair(nested + 1) {
            self.elements[1].add_to(0, right);
            return Some((left, 0));
        }

        if let Some((left, right)) = self.elements[1].explodes(nested + 1) {
            self.elements[1] = Element::Regular(0);
            self.elements[0].add_to(1, left);
            return Some((0, right));
        }

        if let Some((left, right)) = self.elements[1].explode_subpair(nested + 1) {
            self.elements[0].add_to(1, left);
            return Some((0, right));
        }

        None
    }

    fn add_to(&mut self, index: usize, v: u32) {
        self.elements[index].add_to(index, v);
    }

    fn split(&mut self) -> bool {
        self.elements.iter_mut()
            .any(Element::split)
    }
}

impl Display for SnailfishNumber {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("[{},{}]", self.elements[0], self.elements[1]))
    }
}

struct ParsedInput {
    entries: Vec<SnailfishNumber>,
}

fn parse_regular_number(c: char) -> u32 {
    c.to_digit(10).unwrap()
}

fn parse_element<T: Iterator<Item = char>>(it: &mut T) -> Element {
    let c = it.next().unwrap();
    match c {
        '0'..='9' => Element::Regular(parse_regular_number(c)),
        '[' => Element::Snailfish(Box::new(parse_snailfish_number(it))),
        c => panic!("Unexpected character: {}", c),
    }
}

fn parse_snailfish_number<T: Iterator<Item = char>>(it: &mut T) -> SnailfishNumber {
    let first = parse_element(it);
    assert_eq!(it.next().unwrap(), ',');
    let second = parse_element(it);
    assert_eq!(it.next().unwrap(), ']');
    SnailfishNumber {
        elements: [first, second],
    }
}

fn parse_input(file: &str) -> std::io::Result<ParsedInput> {
    let file = std::fs::File::open(format!("{}/{}", env!("CARGO_PKG_NAME"), file))?;
    let reader = BufReader::new(file);
    let entries = reader.lines()
        .map(Result::unwrap)
        .map(|line| parse_snailfish_number(&mut line.chars().skip(1)))
        .collect();

    Ok(ParsedInput {
        entries,
    })
}

fn reduce_snailfish_number(mut number: SnailfishNumber) -> SnailfishNumber {
    loop {
        if let Some(_) = number.explode_subpair(0) {
            continue;
        }

        if number.split() {
            continue;
        }

        break;
    }

    number
}

fn add_snailfish_numbers(first: &SnailfishNumber, second: &SnailfishNumber) -> SnailfishNumber {
    let raw_sum = SnailfishNumber {
        elements: [
            Element::Snailfish(Box::new(first.clone())),
            Element::Snailfish(Box::new(second.clone())),
        ],
    };

    reduce_snailfish_number(raw_sum)
}

fn part1(input: &ParsedInput) -> usize {
    let fsn = input.entries.iter()
        .skip(1)
        .fold(input.entries.first().unwrap().clone(),
            |first, second| add_snailfish_numbers(&first, second));
    
    fsn.magnitude()
}

fn part2(input: &ParsedInput) -> usize {
    input.entries.iter()
        .map(|left| input.entries.iter()
            .filter(|right| *left != **right)
            .map(|right| add_snailfish_numbers(left, right).magnitude()))
        .flatten()
        .max()
        .unwrap()
}

fn main() {
    let test_input = parse_input("test.txt").unwrap();
    assert_eq!(part1(&test_input), 4140);
    
    let parsed_input = parse_input("input.txt").unwrap();
    println!("{}", part1(&parsed_input));

    assert_eq!(part2(&test_input), 3993);
    println!("{}", part2(&parsed_input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn reduce_test() {
        let sn = parse_snailfish_number(&mut "[[[[[9,8],1],2],3],4]".chars().skip(1));
        assert_eq!(format!("{}", reduce_snailfish_number(sn)), String::from("[[[[0,9],2],3],4]"));

        let sn = parse_snailfish_number(&mut "[7,[6,[5,[4,[3,2]]]]]".chars().skip(1));
        assert_eq!(format!("{}", reduce_snailfish_number(sn)), String::from("[7,[6,[5,[7,0]]]]"));

        let sn = parse_snailfish_number(&mut "[[6,[5,[4,[3,2]]]],1]".chars().skip(1));
        assert_eq!(format!("{}", reduce_snailfish_number(sn)), String::from("[[6,[5,[7,0]]],3]"));
    }

    #[test]
    fn add_test() {
        let left = parse_snailfish_number(&mut "[[[[4,3],4],4],[7,[[8,4],9]]]".chars().skip(1));
        let right = parse_snailfish_number(&mut "[1,1]".chars().skip(1));
        assert_eq!(format!("{}", add_snailfish_numbers(&left, &right)), String::from("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]"));
    }
}
use std::io::{BufRead, BufReader};
use std::fs::File;

fn parse_file() -> std::io::Result<Vec<u16>> {
    let file = File::open("day3/input.txt")?;
    let reader = BufReader::new(file);
    reader.lines().map(|line| line.map(|line| u16::from_str_radix(&line, 2).unwrap())).collect()
}

fn part1(values: &[u16]) {
    let gamma: u16 = (0..16u16).map(|i| values.iter().map(|v| (v >> i) & 0x01).sum::<u16>() * 2u16 / u16::try_from(values.len()).unwrap() << i).sum();
    let epsilon = !gamma & 0x0fff;
    println!("{}", u32::from(gamma) * u32::from(epsilon));
}

fn part2(values: &[u16]) {
    let oxygen = *(0..12).rev().fold(Vec::from(values), |values_for_oxygen, i| {
        if values_for_oxygen.len() <= 1 {
            return values_for_oxygen;
        }

        let ones_count = usize::from(values_for_oxygen.iter().map(|v| (v >> i) & 0x01).sum::<u16>());
        let oxygen_mask = ((values_for_oxygen.first().unwrap() >> (i + 1)) << (i + 1)) | if ones_count * 2 >= values_for_oxygen.len() { 1 << i } else { 0 };
        values_for_oxygen.into_iter().filter(|&v| ((v >> i) << i) == oxygen_mask).collect()
    }).first().unwrap();

    let scrubber = *(0..12).rev().fold(Vec::from(values), |values_for_scrubber, i| {
        if values_for_scrubber.len() <= 1 {
            return values_for_scrubber;
        }

        let ones_count = usize::from(values_for_scrubber.iter().map(|v| (v >> i) & 0x01).sum::<u16>());
        let scrubber_mask = ((values_for_scrubber.first().unwrap() >> (i + 1)) << (i + 1)) | if ones_count * 2 >= values_for_scrubber.len() { 0 } else { 1 << i };
        values_for_scrubber.into_iter().filter(|&v| ((v >> i) << i) == scrubber_mask).collect()
    }).first().unwrap();

    println!("{}", u32::from(oxygen) * u32::from(scrubber));
}

fn main() {
    let values = parse_file().unwrap();
    part1(&values);
    part2(&values);
}

#!/bin/sh

AOC_SESSION_ID_PATH=".local/AOC_SESSION_ID"

if [ -f $AOC_SESSION_ID_PATH ]; then
    AOC_SESSION_ID=$(cat $AOC_SESSION_ID_PATH)
else
    if [ -z $AOC_SESSION_ID ]; then
        echo "Missing env AOC_SESSION_ID"
        exit 1
    fi

    if [ -z $1 ]; then
        echo "Missing day number"
        exit 1
    fi
fi

DAY=$1
COOKIE="session=$AOC_SESSION_ID"

curl --cookie "$COOKIE" https://adventofcode.com/2021/day/$DAY/input > day$DAY/input.txt

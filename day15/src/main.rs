use std::{io::{BufReader, BufRead}, fmt::Display};

#[derive(Clone, PartialEq, Eq, Hash)]
struct Point {
    x: i32,
    y: i32,
}

impl Point {
    const fn new(x: i32, y: i32) -> Point {
        Point { x, y }
    }

    const fn translate(&self, offset: &Point) -> Point {
        Point {
            x: self.x + offset.x,
            y: self.y + offset.y,
        }
    }
}

impl Display for Point {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("({}, {})", self.x, self.y))
    }
}

struct RiskMap {
    size: Point,
    cells: Vec<usize>,
}

const RELATIVE_ADJACENTS: &[Point] = &[
    Point::new( 1,  0),
    Point::new( 0,  1),
    Point::new(-1,  0),
    Point::new( 0, -1),
];

impl RiskMap {
    fn with_size(size: &Point, value: usize) -> Self {
        Self {
            size: size.clone(),
            cells: std::iter::repeat(value).take((size.x as usize) * (size.y as usize)).collect(),
        }
    }

    fn from_cells(cells: Vec<Vec<usize>>) -> RiskMap {
        RiskMap {
            size: Point {
                x: cells[0].len() as i32,
                y: cells.len() as i32,
            },
            cells: cells.into_iter()
                .flatten()
                .collect(),
        }
    }

    fn get(&self, p: &Point) -> Option<usize> {
        if p.x < 0 || p.y < 0 || p.x >= self.size.x || p.y >= self.size.y {
            return None;
        }

        let index = (p.y * self.size.x + p.x) as usize;
        Some(self.cells[index])
    }

    fn get_mut(&mut self, p: &Point) -> Option<&mut usize> {
        if p.x < 0 || p.y < 0 || p.x >= self.size.x || p.y >= self.size.y {
            return None;
        }

        let index = (p.y * self.size.x + p.x) as usize;
        Some(&mut self.cells[index])
    }
}

struct ParsedInput {
    map: RiskMap,
}

fn parse_row<T: AsRef<str>>(line: T) -> Vec<usize> {
    line.as_ref()
        .chars()
        .map(|c| c.to_digit(10).unwrap() as usize)
        .collect()
}

fn parse_input(file: &str) -> std::io::Result<ParsedInput> {
    let file = std::fs::File::open(format!("{}/{}", env!("CARGO_PKG_NAME"), file))?;
    let reader = BufReader::new(file);
    let entries = reader.lines()
        .map(Result::unwrap)
        .map(parse_row)
        .collect();

    Ok(ParsedInput {
        map: RiskMap::from_cells(entries),
    })
}

fn find_shortest(map: &RiskMap) -> usize {
    let start = Point::new(0, 0);
    let end = map.size.translate(&Point::new(-1, -1));

    let mut sum_map = RiskMap::with_size(&map.size, usize::MAX);
    *sum_map.get_mut(&end).unwrap() = 0;

    let mut changed = Vec::from([end.clone()]);
    let mut newly_changed = Vec::new();

    while !changed.is_empty() {
        newly_changed.clear();

        for p in changed.iter().map(|p| RELATIVE_ADJACENTS.iter()
            .map(|rel| p.translate(rel))).flatten() {
            if let Some(dist) = sum_map.get(&p) {
                let closest = RELATIVE_ADJACENTS.iter()
                    .map(|rel| p.translate(rel))
                    .filter_map(|adjacent| sum_map.get(&adjacent)
                        .filter(|d| *d < usize::MAX)
                        .map(|d| map.get(&adjacent).unwrap() + d))
                    .min();
                
                if let Some(closest) = closest {
                    if closest < dist {
                        *sum_map.get_mut(&p).unwrap() = closest;
                        newly_changed.push(p);
                    }
                }
            }
        }

        std::mem::swap(&mut changed, &mut newly_changed);
    }

    sum_map.get(&start).unwrap()
}

fn part1(input: &ParsedInput) -> usize {
    find_shortest(&input.map)
}

fn get_repeated(map: &RiskMap, p: &Point) -> usize {
    let map_point = Point::new(p.x % map.size.x, p.y % map.size.y);
    let risk = map.get(&map_point).unwrap();

    let x_repeated = (p.x / map.size.x) as usize;
    let y_repeated = (p.y / map.size.y) as usize;
    ((risk + x_repeated + y_repeated - 1) % 9) + 1
}

fn make_big_map(map: &RiskMap) -> RiskMap {
    let size = 5;
    let width = size * map.size.x;
    let height = size * map.size.y;

    RiskMap::from_cells(
        (0..height)
            .map(|y|
                (0..width)
                    .map(|x| get_repeated(&map, &Point::new(x, y)))
                    .collect())
            .collect())
}

fn part2(input: &ParsedInput) -> usize {
    let big_map = make_big_map(&input.map);
    find_shortest(&big_map)
}

fn main() {
    let test_input = parse_input("test.txt").unwrap();
    assert_eq!(part1(&test_input), 40);
    
    let parsed_input = parse_input("input.txt").unwrap();
    println!("{}", part1(&parsed_input));

    assert_eq!(part2(&test_input), 315);
    println!("{}", part2(&parsed_input));
}

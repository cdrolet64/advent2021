use std::{io::{BufReader, BufRead}, fmt::Display, collections::{HashMap, HashSet}};

#[derive(PartialEq, Eq, Clone)]
enum PixelState {
    Light,
    Dark,
}

impl PixelState {
    fn to_value(&self) -> u16 {
        match self {
            Self::Light => 0x01,
            Self::Dark  => 0x00,
        }
    }
}

impl From<char> for PixelState {
    fn from(c: char) -> Self {
        match c {
            '#' => Self::Light,
            '.' => Self::Dark,
            _ => unreachable!(),
        }
    }
}

#[derive(Clone, PartialEq, Eq, Hash)]
struct Point {
    x: i32,
    y: i32,
}

impl Point {
    const fn new(x: i32, y: i32) -> Point {
        Point { x, y }
    }

    const fn translate(&self, offset: &Point) -> Point {
        Point {
            x: self.x + offset.x,
            y: self.y + offset.y,
        }
    }
}

impl Display for Point {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("({}, {})", self.x, self.y))
    }
}

fn relative_neighbors<'a>(p: &'a Point) -> impl Iterator<Item = Point> + 'a {
    (-1..=1).map(move |y|
        (-1..=1).map(move |x| p.translate(&Point::new(x, y))))
        .flatten()
}

#[derive(Clone)]
struct Image {
    pixels: HashMap<Point, PixelState>,
    others: PixelState,
}

impl Image {
    fn from_matrix<T: Iterator<Item = String>>(it: T, others: PixelState) -> Self {
        Self {
            pixels: it.zip(0..)
                .map(|(it_pixel, y)| it_pixel.chars().zip(0..)
                    .map(|(c, x)| (Point::new(x, y), PixelState::from(c))).collect::<Vec<_>>())
                .flatten()
                .filter(|(_, pixel)| pixel == &PixelState::Light)
                .collect(),
            others,
        }
    }

    fn from_iter<T: Iterator<Item = (Point, PixelState)>>(it: T, others: PixelState) -> Self {
        Self {
            pixels: it.filter(|(_, pixel)| pixel != &others)
                .collect(),
            others,
        }
    }
    
    fn iter(&self) -> impl Iterator<Item = (&Point, &PixelState)> {
        self.pixels.iter()
    }

    fn get(&self, p: &Point) -> &PixelState {
        self.pixels.get(p)
            .unwrap_or(&self.others)
    }

    fn get_others(&self) -> &PixelState {
        &self.others
    }
}

struct ParsedInput {
    algorithm: Vec<PixelState>,
    image: Image,
}

fn parse_algorithm<T: AsRef<str>>(line: T) -> Vec<PixelState> {
    line.as_ref()
        .chars()
        .map(From::from)
        .collect()
}

fn parse_input(file: &str) -> std::io::Result<ParsedInput> {
    let file = std::fs::File::open(format!("{}/{}", env!("CARGO_PKG_NAME"), file))?;
    let reader = BufReader::new(file);

    let mut it_lines = reader.lines()
        .map(Result::unwrap);

    let algorithm = parse_algorithm(it_lines.next().unwrap());
    assert!(it_lines.next().unwrap().is_empty());

    let image = Image::from_matrix(it_lines, PixelState::Dark);

    Ok(ParsedInput {
        algorithm,
        image,
    })
}

fn get_next_pixel<'a, T: Iterator<Item = &'a PixelState>>(it: T, algorithm: &[PixelState]) -> PixelState {
    let index = it.map(PixelState::to_value)
        .reduce(|v, c| (v << 1) | c)
        .unwrap();

    algorithm[index as usize].clone()
}

fn get_neighbor_values<'a>(point: &'a Point, image: &'a Image) -> impl Iterator<Item = &'a PixelState> {
    relative_neighbors(point)
        .map(|neighbor| image.get(&neighbor))
}

fn next_image(image: &Image, algorithm: &[PixelState]) -> Image {
    let to_process: HashSet<_> = image.iter()
        .map(|(point, _)| relative_neighbors(point))
        .flatten()
        .collect();

    Image::from_iter(to_process.iter()
        .map(|point| (point.clone(), get_next_pixel(get_neighbor_values(point, image), algorithm))),
        get_next_pixel(std::iter::repeat(image.get_others()).take(9), algorithm))
}

fn count_light_pixels(image: &Image) -> usize {
    assert!(image.get_others() != &PixelState::Light);

    image.iter()
        .filter(|(_, pixel)| pixel == &&PixelState::Light)
        .count()
}

fn enhance_multiple_times(image: &Image, algorithm: &[PixelState], count: usize) -> Image {
    let mut image = image.clone();
    for _ in 0..count {
        image = next_image(&image, &algorithm);
    }

    image
}

fn part1(input: &ParsedInput) -> usize {
    let enhanced_image = enhance_multiple_times(&input.image, &input.algorithm, 2);
    count_light_pixels(&enhanced_image)
}

fn part2(input: &ParsedInput) -> usize {
    let enhanced_image = enhance_multiple_times(&input.image, &input.algorithm, 50);
    count_light_pixels(&enhanced_image)
}

fn main() {
    let test_input = parse_input("test.txt").unwrap();
    assert_eq!(part1(&test_input), 35);
    
    let parsed_input = parse_input("input.txt").unwrap();
    println!("{}", part1(&parsed_input));

    assert_eq!(part2(&test_input), 3351);
    println!("{}", part2(&parsed_input));
}

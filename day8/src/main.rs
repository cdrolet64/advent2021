use std::collections::BTreeSet;
use std::io::{BufReader, BufRead};

struct InputEntry {
    digits: Vec<String>,
    output: Vec<String>,
}

struct ParsedInput {
    entries: Vec<InputEntry>,
}

fn parse_digits(text: &str) -> Vec<String> {
    text.split(' ')
        .map(ToString::to_string)
        .collect()
}

fn parse_entry<T: AsRef<str>>(line: T) -> InputEntry {
    let mut it = line.as_ref()
        .split(" | ")
        .map(parse_digits);

    InputEntry {
        digits: it.next().unwrap(),
        output: it.next().unwrap(),
    }
}

fn parse_input(file: &str) -> std::io::Result<ParsedInput> {
    let file = std::fs::File::open(format!("day8/{}", file))?;
    let reader = BufReader::new(file);
    let entries = reader.lines()
        .map(Result::unwrap)
        .map(parse_entry)
        .collect();

    Ok(ParsedInput {
        entries,
    })
}

fn count_1_4_7_8(output: &[String]) -> usize {
    const UNIQUE_DIGITS_LEN: &[usize] = &[
        2, // 1
        3, // 7
        4, // 4
        7, // 8
    ];

    output.iter()
        .filter(|digit| UNIQUE_DIGITS_LEN.contains(&digit.len()))
        .count()
}

fn part1(input: &ParsedInput) -> usize {
    input.entries.iter()
        .map(|entry| count_1_4_7_8(&entry.output))
        .sum()
}

#[derive(PartialEq, Eq, Clone, PartialOrd, Ord)]
struct Digit {
    segments: BTreeSet<char>,
}

impl Digit {
    fn from_str(digit_text: &str) -> Self {
        Self {
            segments: digit_text.chars().collect(),
        }
    }

    fn segments_count(&self) -> usize {
        self.segments.len()
    }

    fn has_all_segments(&self, digit: &Digit) -> bool {
        self.segments.is_superset(&digit.segments)
    }
}

fn find_unique_digit<'a, T: Iterator<Item = &'a Digit>>(mut digits: T, segments_count: usize) -> &'a Digit {
    digits.find(|d| d.segments_count() == segments_count)
        .unwrap()
}

fn find_1<'a>(digits: &'a [Digit]) -> &'a Digit {
    find_unique_digit(digits.iter(), 2)
}

fn find_4<'a>(digits: &'a [Digit]) -> &'a Digit {
    find_unique_digit(digits.iter(), 4)
}

fn find_7<'a>(digits: &'a [Digit]) -> &'a Digit {
    find_unique_digit(digits.iter(), 3)
}

fn find_8<'a>(digits: &'a [Digit]) -> &'a Digit {
    find_unique_digit(digits.iter(), 7)
}

fn find_9<'a>(digits: &'a [Digit], four: &Digit, eight: &Digit) -> &'a Digit {
    digits.iter()
        .filter(|&d| d != four && d != eight)
        .find(|d| d.has_all_segments(four))
        .unwrap()
}

fn find_0_and_3<'a>(digits: &'a [Digit], seven: &Digit, eight: &Digit, nine: &Digit) -> (&'a Digit, &'a Digit) {
    let zero_and_three: Vec<_> = digits.iter()
        .filter(|&d| d != seven && d != eight && d != nine)
        .filter(|&d| d.has_all_segments(seven))
        .collect();

    assert!(zero_and_three.len() == 2);

    let zero = find_unique_digit(zero_and_three.iter().map(|d| *d), 6);
    let three = find_unique_digit(zero_and_three.iter().map(|d| *d), 5);
    (zero, three)
}

fn find_6<'a>(digits: &'a [Digit], zero: &Digit, nine: &Digit) -> &'a Digit {
    digits.iter()
        .filter(|&d| d != zero && d != nine)
        .find(|&d| d.segments_count() == 6)
        .unwrap()
}

fn find_2_and_5<'a>(digits: &'a [Digit], three: &Digit, nine: &Digit) -> (&'a Digit, &'a Digit) {
    let two_and_five: Vec<_> = digits.iter()
        .filter(|&d| d.segments_count() == 5)
        .filter(|&d| d != three)
        .collect();

    let five = two_and_five.iter()
        .map(|d| *d)
        .find(|d| nine.has_all_segments(d))
        .unwrap();

    let two = two_and_five.iter()
        .map(|d| *d)
        .find(|d| !nine.has_all_segments(d))
        .unwrap();
    
    (two, five)
}

fn convert_to_digits(digits: &[String]) -> Vec<Digit> {
    digits.iter()
        .map(|digit_text| Digit::from_str(&digit_text))
        .collect()
}

fn resolve_digits<'a>(digits: &'a [Digit]) -> [&'a Digit; 10] {
    let one = find_1(digits);
    let four = find_4(digits);
    let seven = find_7(digits);
    let eight = find_8(digits);
    let nine = find_9(digits, four, eight);
    let (zero, three) = find_0_and_3(digits, seven, eight, nine);
    let six = find_6(digits, zero, nine);
    let (two, five) = find_2_and_5(digits, three, nine);

    let ordered_digits = [ zero, one, two, three, four, five, six, seven, eight, nine ];

    let mut to_check = ordered_digits.to_vec();
    to_check.sort();
    to_check.dedup();
    assert_eq!(to_check.len(), 10);

    ordered_digits
}

fn get_digit_value(output_digit: &Digit, ordered_digits: &[&Digit]) -> usize {
    ordered_digits.iter()
        .enumerate()
        .find(|(_, digit)| **digit == output_digit)
        .unwrap()
        .0
}

fn compute_output(output_digits: &[Digit], ordered_digits: &[&Digit]) -> usize {
    output_digits.iter()
        .map(|output_digit| get_digit_value(output_digit, ordered_digits))
        .fold(0, |number, digit| number * 10 + digit)
}

fn resolve_entry(entry: &InputEntry) -> usize {
    let digits = convert_to_digits(&entry.digits);
    let ordered_digits = resolve_digits(&digits);
    
    let output_digits = convert_to_digits(&entry.output);

    compute_output(&output_digits, &ordered_digits)
}

fn part2(input: &ParsedInput) -> usize {
    input.entries.iter()
        .map(resolve_entry)
        .sum()
}

fn main() {
    let test_input = parse_input("test.txt").unwrap();
    assert_eq!(part1(&test_input), 26);
    
    let parsed_input = parse_input("input.txt").unwrap();
    println!("{}", part1(&parsed_input));

    assert_eq!(part2(&test_input), 61229);
    println!("{}", part2(&parsed_input));
}

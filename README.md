Run:
```
cargo run --bin day[N]
```

Run all:
```
./run_all.sh
```

Create a new day:
```
./make_new.sh [N]
```

use std::{io::{BufReader, BufRead}, collections::HashMap};

type Rules = HashMap<[char; 2], char>;
type Counts = HashMap<char, usize>;
type AdvancedRules = HashMap<[char; 2], (char, Counts)>;

struct ParsedInput {
    template: Vec<char>,
    rules: Rules,
}

fn parse_rule<T: AsRef<str>>(line: T) -> ([char; 2], char) {
    let mut it = line.as_ref()
        .split(" -> ");

    let mut it_polymer = it.next().unwrap().chars();
    let polymer = [it_polymer.next().unwrap(), it_polymer.next().unwrap()];
    let insert = it.next().unwrap().chars().next().unwrap();
    (polymer, insert)
}

fn parse_input(file: &str) -> std::io::Result<ParsedInput> {
    let file = std::fs::File::open(format!("{}/{}", env!("CARGO_PKG_NAME"), file))?;
    let reader = BufReader::new(file);

    let mut lines = reader.lines()
        .map(Result::unwrap);

    let template = lines.next().unwrap().chars().collect();
    let rules = lines.skip(1)
        .map(parse_rule)
        .collect();

    Ok(ParsedInput {
        template,
        rules,
    })
}

fn merge_counts(a: &Counts, b: &Counts) -> Counts {
    let mut c = a.clone();

    for (ch, count) in b {
        let e = c.entry(*ch).or_default();
        *e += count;
    }

    c
}

fn advance_rules(source: &AdvancedRules) -> AdvancedRules {
    source.iter()
        .map(|(polymer, (insert, _counts))| {
            let counts = merge_counts(
                &source.get(&[polymer[0], *insert]).unwrap().1,
                &source.get(&[*insert, polymer[1]]).unwrap().1);
            
            (polymer.clone(), (*insert, counts))
        })
        .collect()
}

fn solve_with_advanced_rules(polymer: &[char], rules: &AdvancedRules) -> Counts {
    polymer.windows(2)
        .map(|polymer| &rules.get(polymer).unwrap().1)
        .fold(Counts::from([(polymer[0], 1)]), |counts, current| merge_counts(&counts, current))
}

fn make_counts(c2: char, c3: char) -> Counts {
    merge_counts(&Counts::from([(c2, 1)]), &Counts::from([(c3, 1)]))
}

fn make_advanced_rules(rules: &Rules) -> AdvancedRules {
    rules.iter()
        .map(|rule| (rule.0.clone(), (rule.1.clone(), make_counts(rule.0[1], *rule.1))))
        .collect()
}

fn solve(polymer: &[char], rules: &Rules, iterations: usize) -> usize{
    let advanced_rules = (0..(iterations - 1))
        .fold(make_advanced_rules(rules), |advanced_rules, _| advance_rules(&advanced_rules));

    let counts = solve_with_advanced_rules(polymer, &advanced_rules);
    let max = counts.iter().map(|x| *x.1).max().unwrap();
    let min = counts.iter().map(|x| *x.1).min().unwrap();

    max - min
}

fn part1(input: &ParsedInput) -> usize {
    solve(&input.template, &input.rules, 10)
}

fn part2(input: &ParsedInput) -> usize {
    solve(&input.template, &input.rules, 40)
}

fn main() {
    let test_input = parse_input("test.txt").unwrap();
    assert_eq!(part1(&test_input), 1588);
    
    let parsed_input = parse_input("input.txt").unwrap();
    println!("{}", part1(&parsed_input));

    assert_eq!(part2(&test_input), 2188189693529);
    println!("{}", part2(&parsed_input));
}

#!/bin/sh

DAY=$1

if [ -z $1 ]; then
    echo "Missing day number"
    exit 1
fi

if [ $1 -lt 1 ] || [ $1 -gt 25 ]; then
    echo "Invalid day number"
    exit 1
fi

DAY_NAME="day$DAY"

if [ -d $DAY_NAME ]; then
    echo "Day already created"
    exit 1
fi

cargo new $DAY_NAME
cp template/main.rs $DAY_NAME/src/main.rs

echo > $DAY_NAME/test.txt
./get_input.sh $1

code -r $DAY_NAME/test.txt $DAY_NAME/src/main.rs

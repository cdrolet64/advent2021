use std::{io::{BufReader, BufRead}, fmt::Display};

#[derive(Clone, PartialEq, Eq, Hash)]
struct Point {
    x: i32,
    y: i32,
}

impl Point {
    const fn new(x: i32, y: i32) -> Point {
        Point { x, y }
    }

    const fn translate(&self, offset: &Point) -> Point {
        Point {
            x: self.x + offset.x,
            y: self.y + offset.y,
        }
    }
}

impl Display for Point {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("({}, {})", self.x, self.y))
    }
}

const RELATIVE_NEIGHBORS: &[Point] = &[
    Point::new( 1,  0),
    Point::new( 1,  1),
    Point::new( 0,  1),
    Point::new(-1,  1),
    Point::new(-1,  0),
    Point::new(-1, -1),
    Point::new( 0, -1),
    Point::new( 1, -1),
];

#[derive(Clone)]
struct OctopusMap {
    size: Point,
    cells: Vec<usize>,
}

impl OctopusMap {
    fn from_cells(cells: Vec<Vec<usize>>) -> OctopusMap {
        OctopusMap {
            size: Point {
                x: cells[0].len() as i32,
                y: cells.len() as i32,
            },
            cells: cells.into_iter()
                .flatten()
                .collect(),
        }
    }

    fn get(&self, p: &Point) -> Option<usize> {
        if p.x < 0 || p.y < 0 || p.x >= self.size.x || p.y >= self.size.y {
            return None;
        }

        let index = (p.y * self.size.x + p.x) as usize;
        Some(self.cells[index])
    }

    fn get_mut(&mut self, p: &Point) -> Option<&mut usize> {
        if p.x < 0 || p.y < 0 || p.x >= self.size.x || p.y >= self.size.y {
            return None;
        }

        let index = (p.y * self.size.x + p.x) as usize;
        Some(&mut self.cells[index])
    }

    fn iter<'a>(&'a self) -> impl Iterator<Item = (Point, usize)> + 'a {
        self.cells.iter()
            .enumerate()
            .map(|(i, v)| (Point::new((i as i32) % self.size.x, (i as i32) / self.size.x), *v))
    }

    fn increment(&mut self) {
        self.cells.iter_mut()
            .for_each(|c| *c += 1);
    }

    fn flash(&mut self) -> usize {
        let flashing: Vec<_> = self.iter()
            .filter(|(_p, v)| *v > 9)
            .map(|(p, _)| p)
            .collect();
        
        let flashing_count = flashing.len();

        let incrementing: Vec<_> = flashing.iter()
            .map(|flashing| RELATIVE_NEIGHBORS.iter()
                .map(|relative_neighbor| flashing.translate(relative_neighbor))
                .filter(|neighbor| self.get(neighbor).map(|v| v > 0).unwrap_or(false)))
            .flatten()
            .collect();

        incrementing.iter()
            .for_each(|p| {
                *self.get_mut(p).unwrap() += 1;
            });

        flashing.into_iter()
            .for_each(|p| {
                *self.get_mut(&p).unwrap() = 0;
            });

        flashing_count
    }
}

struct ParsedInput {
    map: OctopusMap,
}

fn parse_row<T: AsRef<str>>(line: T) -> Vec<usize> {
    line.as_ref()
        .chars()
        .map(|c| c.to_digit(10).unwrap() as usize)
        .collect()
}

fn parse_input(file: &str) -> std::io::Result<ParsedInput> {
    let file = std::fs::File::open(format!("{}/{}", env!("CARGO_PKG_NAME"), file))?;
    let reader = BufReader::new(file);
    let cells = reader.lines()
        .map(Result::unwrap)
        .map(parse_row)
        .collect();

    Ok(ParsedInput {
        map: OctopusMap::from_cells(cells),
    })
}

fn part1(input: &ParsedInput) -> usize {
    let mut map = input.map.clone();

    let mut flashes = 0;
    for _ in 0..100 {
        map.increment();
        let mut new_flashes = 1;
        while new_flashes > 0 {
            new_flashes = map.flash();
            flashes += new_flashes;
        }
    }

    flashes
}

fn part2(input: &ParsedInput) -> usize {
    let mut map = input.map.clone();

    const MAX_STEPS: usize = 100000;

    for i in 1..MAX_STEPS {
        let mut flashes = 0;

        map.increment();
        let mut new_flashes = 1;
        while new_flashes > 0 {
            new_flashes = map.flash();
            flashes += new_flashes;
        }

        if flashes == map.iter().count() {
            return i;
        }
    }
    
    panic!("Not all flashing after {} steps!", MAX_STEPS);
}

fn main() {
    let test_input = parse_input("test.txt").unwrap();
    assert_eq!(part1(&test_input), 1656);
    
    let parsed_input = parse_input("input.txt").unwrap();
    println!("{}", part1(&parsed_input));

    assert_eq!(part2(&test_input), 195);
    println!("{}", part2(&parsed_input));
}

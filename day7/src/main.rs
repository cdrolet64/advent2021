use std::str::FromStr;
use std::io::{BufReader, BufRead};

struct ParsedInput {
    positions: Vec<usize>,
}

fn parse_input(file: &str) -> std::io::Result<ParsedInput> {
    let file = std::fs::File::open(format!("day7/{}", file))?;
    let reader = BufReader::new(file);
    let positions = reader.lines()
        .map(|line| line.unwrap().split(',').map(|n| usize::from_str(n).unwrap()).collect::<Vec<_>>())
        .flatten()
        .collect();
        

    Ok(ParsedInput {
        positions,
    })
}

fn part1(input: &ParsedInput) -> usize {
    let candidates: std::collections::BTreeSet<_> = input.positions.iter().collect();
    candidates.iter()
        .map(|&&c| (c, input.positions.iter()
            .map(|&p| usize::try_from((isize::try_from(p).unwrap() - isize::try_from(c).unwrap()).abs()).unwrap())
            .sum::<usize>()))
        .min_by_key(|(_, f)| *f).unwrap().1
}

fn fuel_cost(distance: usize) -> usize {
    distance * (distance + 1) / 2
}

fn part2(input: &ParsedInput) -> usize {
    let candidates = (*input.positions.iter().min().unwrap())..=(*input.positions.iter().max().unwrap());
    candidates.map(|c| (c, input.positions.iter()
            .map(|&p| fuel_cost(usize::try_from((isize::try_from(p).unwrap() - isize::try_from(c).unwrap()).abs()).unwrap()))
            .sum::<usize>()))
        .min_by_key(|(_, f)| *f).unwrap().1
}

fn main() {
    let test_input = parse_input("test.txt").unwrap();
    assert_eq!(part1(&test_input), 37);
    
    let parsed_input = parse_input("input.txt").unwrap();
    println!("{}", part1(&parsed_input));

    assert_eq!(part2(&test_input), 168);
    println!("{}", part2(&parsed_input));
}

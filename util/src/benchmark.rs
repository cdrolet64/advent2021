use std::fmt::Display;

pub struct Benchmark<T: Display> {
    name: T,
    start: std::time::Instant,
}

impl<T: Display> Benchmark<T> {
    pub fn start(name: T) -> Self {
        Self {
            name,
            start: std::time::Instant::now(),
        }
    }
}

impl<T: Display> Drop for Benchmark<T> {
    fn drop(&mut self) {
        let end = std::time::Instant::now();
        println!("{}: {} \u{00B5}s", self.name, (end - self.start).as_micros());
    }
}

use std::{io::{BufReader, BufRead}, fmt::Display};

#[derive(Clone, PartialEq, Eq, Hash)]
struct Point {
    x: i32,
    y: i32,
}

impl Point {
    const fn new(x: i32, y: i32) -> Point {
        Point { x, y }
    }

    const fn translate(&self, offset: &Point) -> Point {
        Point {
            x: self.x + offset.x,
            y: self.y + offset.y,
        }
    }
}

impl Display for Point {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("({}, {})", self.x, self.y))
    }
}

#[derive(Clone, PartialEq, Eq)]
enum SeaCucumber {
    East,
    South,
}

#[derive(Clone, PartialEq, Eq)]
struct SeaFloor {
    size: Point,
    cells: Vec<Option<SeaCucumber>>,
}

const NORTH: Point = Point::new(0, -1);
const SOUTH: Point = Point::new(0, 1);
const WEST: Point = Point::new(-1, 0);
const EAST: Point = Point::new(1, 0);

impl SeaFloor {
    fn point_from_index(&self, i: usize) -> Point {
        assert!(i < (self.size.x * self.size.y) as usize);
        Point::new(i as i32 % self.size.x, i as i32 / self.size.x)
    }

    fn point_to_index(&self, p: &Point) -> usize {
        let p = Point::new((p.x + self.size.x) % self.size.x, (p.y + self.size.y) % self.size.y);
        (p.y * self.size.x + p.x) as usize
    }

    fn get(&self, p: &Point) -> Option<SeaCucumber> {
        self.cells[self.point_to_index(p)].clone()
    }

    fn iter_cells<'a>(&'a self) -> impl Iterator<Item = (Point, Option<SeaCucumber>)> + 'a {
        self.cells.iter()
            .enumerate()
            .map(|(i, v)| (self.point_from_index(i), v.clone()))
    }

    fn advance_east(&self) -> Self {
        Self {
            size: self.size.clone(),
            cells: self.iter_cells()
                .map(|(p, c)| match c {
                    Some(SeaCucumber::South) => c,
                    Some(SeaCucumber::East) => if self.get(&p.translate(&EAST)).is_none() { None } else { c },
                    None => if self.get(&p.translate(&WEST)).map(|v| v == SeaCucumber::East).unwrap_or(false) { Some(SeaCucumber::East) } else { c },
                })
                .collect()
        }
    }

    fn advance_south(&self) -> Self {
        Self {
            size: self.size.clone(),
            cells: self.iter_cells()
                .map(|(p, c)| match c {
                    Some(SeaCucumber::South) => if self.get(&p.translate(&SOUTH)).is_none() { None } else { c },
                    Some(SeaCucumber::East) => c,
                    None => if self.get(&p.translate(&NORTH)).map(|v| v == SeaCucumber::South).unwrap_or(false) { Some(SeaCucumber::South) } else { c },
                })
                .collect()
        }
    }

    fn advance_all(&self) -> Self {
        self.advance_east()
            .advance_south()
    }

    fn from_iter(cells: &Vec<Vec<Option<SeaCucumber>>>) -> SeaFloor {
        SeaFloor {
            size: Point {
                x: cells[0].len() as i32,
                y: cells.len() as i32,
            },
            cells: cells.into_iter()
                .flatten()
                .cloned()
                .collect(),
        }
    }
}

struct ParsedInput {
    entries: Vec<Vec<Option<SeaCucumber>>>,
}

fn parse_line<T: AsRef<str>>(line: T) -> Vec<Option<SeaCucumber>> {
    line.as_ref()
        .chars()
        .map(|c| match c {
            '.' => None,
            '>' => Some(SeaCucumber::East),
            'v' => Some(SeaCucumber::South),
            _ => unreachable!(),
        })
        .collect()
}

fn parse_input(file: &str) -> std::io::Result<ParsedInput> {
    let file = std::fs::File::open(format!("{}/{}", env!("CARGO_PKG_NAME"), file))?;
    let reader = BufReader::new(file);
    let lines = reader.lines()
        .map(Result::unwrap)
        .map(parse_line)
        .collect();

    Ok(ParsedInput {
        entries: lines,
    })
}

fn part1(input: &ParsedInput) -> usize {
    let mut sea_floor = SeaFloor::from_iter(&input.entries);
    let mut step = 0;

    loop {
        step += 1;
        let next_sea_floor = sea_floor.advance_all();
        if next_sea_floor == sea_floor {
            return step;
        }
        sea_floor = next_sea_floor;
    }
}

fn main() {
    let test_input = parse_input("test.txt").unwrap();
    assert_eq!(part1(&test_input), 58);
    
    let parsed_input = parse_input("input.txt").unwrap();
    println!("{}", part1(&parsed_input));
}

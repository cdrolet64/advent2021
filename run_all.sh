#!/bin/sh

cargo build --release --all

for i in $(seq 1 25)
do
    if [ -d day$i ] && [ -e ./target/release/day$i ]; then
        echo "===================="
        echo "Day $i"
        echo "--------------------"
        ./target/release/day$i
    fi
done

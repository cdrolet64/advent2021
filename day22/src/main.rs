use std::{io::{BufReader, BufRead}, fmt::Display, collections::BTreeSet};

#[derive(Clone, PartialEq, Eq, Hash)]
struct Point {
    p: [i32; 3],
}

impl Point {
    const fn new(x: i32, y: i32, z: i32) -> Point {
        Point { p: [x, y, z] }
    }
}

impl Display for Point {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("({}, {}, {})", self.p[0], self.p[1], self.p[2]))
    }
}

#[derive(Clone)]
struct Cube {
    from: Point,
    to: Point,
}

impl Cube {
    fn len(&self, i: usize) -> i32 {
        self.to.p[i] - self.from.p[i] + 1
    }

    fn contains(&self, p: &Point) -> bool {
        (0..3).all(|i| p.p[i] >= self.from.p[i] && p.p[i] <= self.to.p[i])
    }

    fn intersects(&self, cube: &Cube) -> bool {
        (0..3).all(|i| cube.from.p[i] <= self.to.p[i] && cube.to.p[i] >= self.from.p[i])
    }

    fn join(&self, cube: &Cube) -> Cube {
        Cube {
            from: Point::new(self.from.p[0].min(cube.from.p[0]), self.from.p[1].min(cube.from.p[1]), self.from.p[2].min(cube.from.p[2])),
            to: Point::new(self.to.p[0].max(cube.to.p[0]), self.to.p[1].max(cube.to.p[1]), self.to.p[2].max(cube.to.p[2])),
        }
    }
}

struct Instruction {
    state: bool,
    cube: Cube,
}

struct ParsedInput {
    entries: Vec<Instruction>,
}

fn parse_range(text: &str) -> (i32, i32) {
    let mut it = text.split("..").map(|s| s.parse().unwrap());
    (it.next().unwrap(), it.next().unwrap())
}

fn parse_instruction<T: AsRef<str>>(line: T) -> Instruction {
    let mut it = line.as_ref().split(" x=");

    let state = it.next().unwrap() == "on";

    let mut it = it.next().unwrap().split(",y=");
    let (x1, x2) = parse_range(it.next().unwrap());

    let mut it = it.next().unwrap().split(",z=");
    let (y1, y2) = parse_range(it.next().unwrap());
    let (z1, z2) = parse_range(it.next().unwrap());

    assert!(x1 < x2);
    assert!(y1 < y2);
    assert!(z1 < z2);

    Instruction {
        state,
        cube: Cube {
            from: Point::new(x1, y1, z1),
            to: Point::new(x2, y2, z2),
        },
    }
}

fn parse_input(file: &str) -> std::io::Result<ParsedInput> {
    let file = std::fs::File::open(format!("{}/{}", env!("CARGO_PKG_NAME"), file))?;
    let reader = BufReader::new(file);
    let entries = reader.lines()
        .map(Result::unwrap)
        .map(parse_instruction)
        .collect();

    Ok(ParsedInput {
        entries,
    })
}

fn make_splits(instructions: &[Instruction], zone: &Cube) -> Vec<Vec<i32>> {
    (0..3).map(|i|
        instructions.iter()
            .filter(|instruction| zone.intersects(&instruction.cube))
            .map(|instruction| [instruction.cube.from.p[i], instruction.cube.to.p[i] + 1])
            .flatten()
            .filter(|t| t >= &zone.from.p[i] && t <= &zone.to.p[i])
            .chain([zone.from.p[i], zone.to.p[i] + 1])
            .collect::<BTreeSet<_>>()
            .into_iter()
            .collect())
        .collect()
}

fn is_on(p: &Point, instructions: &[Instruction], _zone: &Cube) -> bool {
    instructions.iter()
        .rev()
        .find(|instruction| instruction.cube.contains(p))
        .map(|instruction| instruction.state)
        .unwrap_or(false)
}

fn count_cubes_on(instructions: &[Instruction], zone: &Cube) -> usize {
    let splits = make_splits(instructions, zone);

    splits[0].windows(2)
        .map(|xs| splits[1].windows(2)
            .map(|ys| splits[2].windows(2)
                .filter_map(|zs| (
                    if is_on(&Point::new(xs[0], ys[0], zs[0]), instructions, &zone) {
                        Some(((zs[1] - zs[0]) as usize) * ((ys[1] - ys[0]) as usize) * ((xs[1] - xs[0]) as usize))
                    } else { None }))
                .sum::<usize>())
            .sum::<usize>())
        .sum()
}

fn subsample<'a>(cube: &'a Cube, factor: i32) -> impl Iterator<Item = Cube> + 'a {
    assert!(factor > 0);

    (0..=factor).zip(std::iter::repeat(cube.len(0) / factor))
        .map(move |(dx, step)| (cube.from.p[0] + dx * step, step))
        .map(move |(x, step_x)|
            (0..=factor).zip(std::iter::repeat(cube.len(1) / factor))
                .map(move |(dy, step)| (cube.from.p[1] + dy * step, step))
                .map(move |(y, step_y)|
                    (0..=factor).zip(std::iter::repeat(cube.len(2) / factor))
                        .map(move |(dz, step)| (cube.from.p[2] + dz * step, step))
                        .map(move |(z, step_z)| Cube {
                            from: Point::new(x, y, z),
                            to: Point::new(x + step_x - 1, y + step_y - 1, z + step_z - 1),
                        }))
                .flatten())
        .flatten()
}

fn part1(input: &ParsedInput) -> usize {
    let zone = Cube {
        from: Point::new(-50, -50, -50),
        to: Point::new(50, 50, 50),
    };

    count_cubes_on(&input.entries, &zone)
}

fn part2(input: &ParsedInput) -> usize {
    let big_zone = input.entries.iter()
        .map(|instruction| &instruction.cube)
        .cloned()
        .reduce(|joined, cube| joined.join(&cube))
        .unwrap();

    subsample(&big_zone, 15)
        .map(|sub_zone| count_cubes_on(&input.entries, &sub_zone))
        .sum()
}

fn test_small_input() {
    let small_input = ParsedInput {
        entries: vec![
            Instruction {
                state: true,
                cube: Cube {
                    from: Point::new(10, 10, 10),
                    to: Point::new(12, 12, 12),
                },
            },
            Instruction {
                state: true,
                cube: Cube {
                    from: Point::new(11, 11, 11),
                    to: Point::new(13, 13, 13),
                },
            },
            Instruction {
                state: false,
                cube: Cube {
                    from: Point::new(9, 9, 9),
                    to: Point::new(11, 11, 11),
                },
            },
            Instruction {
                state: true,
                cube: Cube {
                    from: Point::new(10, 10, 10),
                    to: Point::new(10, 10, 10),
                },
            },
        ],
    };

    assert_eq!(part1(&small_input), 39);
}

fn main() {
    test_small_input();

    let test_input = parse_input("test.txt").unwrap();
    assert_eq!(part1(&test_input), 590784);
    
    let parsed_input = parse_input("input.txt").unwrap();
    println!("{}", part1(&parsed_input));

    let test_input = parse_input("test2.txt").unwrap();
    assert_eq!(part1(&test_input), 474140);
    assert_eq!(part2(&test_input), 2758514936282235);
    println!("{}", part2(&parsed_input));
}

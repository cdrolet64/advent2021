use std::str::FromStr;
use std::io::{BufRead, BufReader};
use std::fs::File;

fn parse_file() -> std::io::Result<Vec<u32>> {
    let file = File::open("day1/input.txt")?;
    let reader = BufReader::new(file);
    reader.lines().map(|line| line.map(|line| u32::from_str(&line).unwrap())).collect()
}

fn part1(values: &[u32]) {
    println!("{}", values.windows(2)
        .filter(|w| w[0] < w[1])
        .count());
}

fn part2(values: &[u32]) {
    println!("{}", values.windows(4)
        .filter(|w| w[0] < w[3])
        .count());
}

fn main() {
    let values = parse_file().unwrap();
    part1(&values);
    part2(&values);
}

use std::{io::{BufReader, BufRead}, fmt::Display, collections::HashSet};

#[derive(Clone, PartialEq, Eq, Hash)]
struct Point {
    x: i32,
    y: i32,
}

impl Point {
    const fn new(x: i32, y: i32) -> Point {
        Point { x, y }
    }

    const fn translate(&self, offset: &Point) -> Point {
        Point {
            x: self.x + offset.x,
            y: self.y + offset.y,
        }
    }
}

impl Display for Point {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("({}, {})", self.x, self.y))
    }
}

struct HeightMap {
    size: Point,
    cells: Vec<usize>,
}

const RELATIVE_ADJACENTS: &[Point] = &[
    Point::new( 1,  0),
    Point::new( 0,  1),
    Point::new(-1,  0),
    Point::new( 0, -1),
];

impl HeightMap {
    fn from_cells(cells: Vec<Vec<usize>>) -> HeightMap {
        HeightMap {
            size: Point {
                x: cells[0].len() as i32,
                y: cells.len() as i32,
            },
            cells: cells.into_iter()
                .flatten()
                .collect(),
        }
    }

    fn get(&self, p: &Point) -> Option<usize> {
        if p.x < 0 || p.y < 0 || p.x >= self.size.x || p.y >= self.size.y {
            return None;
        }

        let index = (p.y * self.size.x + p.x) as usize;
        Some(self.cells[index])
    }

    fn iter<'a>(&'a self) -> impl Iterator<Item = (Point, usize)> + 'a {
        self.cells.iter()
            .enumerate()
            .map(|(i, v)| (Point::new((i as i32) % self.size.x, (i as i32) / self.size.x), *v))
    }
}

struct ParsedInput {
    map: HeightMap,
}

fn parse_row<T: AsRef<str>>(line: T) -> Vec<usize> {
    line.as_ref()
        .chars()
        .map(|c| c.to_digit(10).unwrap() as usize)
        .collect()
}

fn parse_input(file: &str) -> std::io::Result<ParsedInput> {
    let file = std::fs::File::open(format!("{}/{}", env!("CARGO_PKG_NAME"), file))?;
    let reader = BufReader::new(file);
    let entries = reader.lines()
        .map(Result::unwrap)
        .map(parse_row)
        .collect();

    Ok(ParsedInput {
        map: HeightMap::from_cells(entries),
    })
}

fn is_lowest_adjacents(map: &HeightMap, p: &Point, height: usize) -> bool {
    RELATIVE_ADJACENTS.iter()
        .map(|relative_adjacent| p.translate(relative_adjacent))
        .filter_map(|adjacent| map.get(&adjacent))
        .all(|adjacent_height| height < adjacent_height)
}

fn get_risk_level(height: usize) -> usize {
    1 + height
}

fn part1(input: &ParsedInput) -> usize {
    input.map.iter()
        .filter(|(p, height)| is_lowest_adjacents(&input.map, p, *height))
        .map(|(_p, height)| get_risk_level(height))
        .sum()
}

fn get_bassin_size(map: &HeightMap, p: &Point) -> usize {
    let mut bassin_cells = HashSet::new();

    let mut potential_cells = Vec::new();
    potential_cells.push(p.clone());

    while potential_cells.len() > 0 {
        let mut new_potential_cells = Vec::new();

        for potential_cell in potential_cells.into_iter() {
            if bassin_cells.insert(potential_cell.clone()) {
                let mut adjacent_cells = RELATIVE_ADJACENTS.iter()
                    .map(|relative_adjacent| potential_cell.translate(relative_adjacent))
                    .filter(|adjacent| map.get(adjacent).map(|v| v < 9).unwrap_or(false))
                    .collect();
                new_potential_cells.append(&mut adjacent_cells);
            }
        }

        potential_cells = new_potential_cells;
    }

    bassin_cells.len()
}

fn part2(input: &ParsedInput) -> usize {
    let mut bassins: Vec<_> = input.map.iter()
        .filter(|(p, height)| is_lowest_adjacents(&input.map, p, *height))
        .map(|(p, _height)| get_bassin_size(&input.map, &p))
        .collect();

    bassins.sort();

    bassins.iter()
        .rev()
        .take(3)
        .fold(1, |v, size| v * size)
}

fn main() {
    let test_input = parse_input("test.txt").unwrap();
    assert_eq!(part1(&test_input), 15);
    
    let parsed_input = parse_input("input.txt").unwrap();
    println!("{}", part1(&parsed_input));

    assert_eq!(part2(&test_input), 1134);
    println!("{}", part2(&parsed_input));
}

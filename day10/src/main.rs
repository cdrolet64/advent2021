use std::io::{BufReader, BufRead};

struct InputEntry {
    text: String,
}

struct ParsedInput {
    entries: Vec<InputEntry>,
}

fn parse_entry<T: AsRef<str>>(line: T) -> InputEntry {
    InputEntry {
        text: line.as_ref().to_string(),
    }
}

fn parse_input(file: &str) -> std::io::Result<ParsedInput> {
    let file = std::fs::File::open(format!("{}/{}", env!("CARGO_PKG_NAME"), file))?;
    let reader = BufReader::new(file);
    let entries = reader.lines()
        .map(Result::unwrap)
        .map(parse_entry)
        .collect();

    Ok(ParsedInput {
        entries,
    })
}

#[derive(PartialEq, Eq)]
enum State {
    Parenthesis,
    Bracket,
    Brace,
    Chevron,
}

impl State {
    fn opening_from(c: &char) -> Option<State> {
        match c {
            '(' => Some(State::Parenthesis),
            '[' => Some(State::Bracket),
            '{' => Some(State::Brace),
            '<' => Some(State::Chevron),
            _ => None,
        }
    }

    fn closing_from(c: &char) -> Option<State> {
        match c {
            ')' => Some(State::Parenthesis),
            ']' => Some(State::Bracket),
            '}' => Some(State::Brace),
            '>' => Some(State::Chevron),
            _ => None,
        }
    }

    fn corrupt_score(&self) -> usize {
        match self {
            State::Parenthesis => 3,
            State::Bracket => 57,
            State::Brace => 1197,
            State::Chevron => 25137,
        }
    }

    fn incomplete_score(&self) -> usize {
        match self {
            State::Parenthesis => 1,
            State::Bracket => 2,
            State::Brace => 3,
            State::Chevron => 4,
        }
    }
}

enum LineState {
    Ok,
    Corrupted(usize),
    Incomplete(Vec<State>),
}

impl LineState {
    fn to_corrupt_score(&self) -> usize {
        match self {
            LineState::Corrupted(score) => *score,
            _ => 0,
        }
    }

    fn to_incomplete_score(&self) -> usize {
        match self {
            LineState::Incomplete(stack) => stack_to_score(&stack),
            _ => 0,
        }
    }
}

#[derive(PartialEq, Eq)]
enum Fitting {
    Opening,
    Closing,
}

enum StackState {
    Valid(Vec<State>),
    Corrupted(State),
}

fn stack_to_score(stack: &[State]) -> usize {
    stack.iter()
        .rev()
        .fold(0, |score, state| score * 5 + state.incomplete_score())
}

fn char_to_state(c: char) -> (Fitting, State) {
    if let Some(opening_state) = State::opening_from(&c) {
        (Fitting::Opening, opening_state)
    } else {
        (Fitting::Closing, State::closing_from(&c).unwrap())
    }
}

fn decode_line(entry: &InputEntry) -> LineState {
    let stack_state = entry.text.chars()
        .map(char_to_state)
        .fold(StackState::Valid(Vec::new()), |stack, fitting_state| {
            if let StackState::Valid(mut stack) = stack {
                if fitting_state.0 == Fitting::Opening {
                    stack.push(fitting_state.1);
                    StackState::Valid(stack)
                } else {
                    let expected_closing = stack.pop();
                    if expected_closing.as_ref() == Some(&fitting_state.1) {
                        StackState::Valid(stack)
                    } else {
                        StackState::Corrupted(fitting_state.1)
                    }
                }
            } else {
                stack
            }
        });
    
    match stack_state {
        StackState::Valid(stack) => {
            if stack.is_empty() {
                LineState::Ok
            } else {
                LineState::Incomplete(stack)
            }
        },
        StackState::Corrupted(state) => {
            LineState::Corrupted(state.corrupt_score())
        }
    }
}

fn part1(input: &ParsedInput) -> usize {
    input.entries.iter()
        .map(decode_line)
        .map(|line_state| line_state.to_corrupt_score())
        .sum()
}

fn part2(input: &ParsedInput) -> usize {
    let mut scores: Vec<_> = input.entries.iter()
        .map(decode_line)
        .map(|line_state| line_state.to_incomplete_score())
        .filter(|score| score != &0)
        .collect();

    scores.sort();
    assert!(scores.len() % 2 == 1);

    scores[scores.len() / 2]
}

fn main() {
    let test_input = parse_input("test.txt").unwrap();
    assert_eq!(part1(&test_input), 26397);
    
    let parsed_input = parse_input("input.txt").unwrap();
    println!("{}", part1(&parsed_input));

    assert_eq!(part2(&test_input), 288957);
    println!("{}", part2(&parsed_input));
}

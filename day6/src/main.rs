use std::str::FromStr;
use std::io::{BufReader, BufRead};

struct ParsedInput {
    ages: Vec<u8>,
}

fn parse_input() -> std::io::Result<ParsedInput> {
    let file = std::fs::File::open("day6/input.txt")?;
    let reader = BufReader::new(file);
    let ages = reader.lines()
        .map(|line| line.unwrap().split(',').map(|n| u8::from_str(n).unwrap()).collect::<Vec<_>>())
        .flatten()
        .collect();
        

    Ok(ParsedInput {
        ages,
    })
}

fn part1(input: &ParsedInput, days: usize) -> usize {
    let mut counts = input.ages.iter().fold(Vec::from([0usize; 9]), |mut counts, v| { counts[usize::from(*v)] += 1; counts });

    for _i in 0..days {
        let zeroes = counts[0];
        counts = counts.into_iter().skip(1).chain(std::iter::once(0)).collect();
        counts[6] += zeroes;
        counts[8] = zeroes;
    }

    counts.iter().sum()
}

fn main() {
    let test_input = ParsedInput { ages: Vec::from([3,4,3,1,2]) };
    assert_eq!(part1(&test_input, 80), 5934);
    assert_eq!(part1(&test_input, 256), 26984457539);

    let parsed_input = parse_input().unwrap();
    println!("{}", part1(&parsed_input, 80));
    println!("{}", part1(&parsed_input, 256));
}
use std::{io::{BufReader, BufRead}};

struct InputLink {
    from: String,
    to: String,
}

struct ParsedInput {
    links: Vec<InputLink>,
}

fn parse_link<T: AsRef<str>>(line: T) -> InputLink {
    let mut it = line.as_ref().split('-');
    InputLink {
        from: it.next().unwrap().to_string(),
        to: it.next().unwrap().to_string(),
    }
}

fn parse_input(file: &str) -> std::io::Result<ParsedInput> {
    let file = std::fs::File::open(format!("{}/{}", env!("CARGO_PKG_NAME"), file))?;
    let reader = BufReader::new(file);
    let links = reader.lines()
        .map(Result::unwrap)
        .map(parse_link)
        .collect();

    Ok(ParsedInput {
        links,
    })
}

#[derive(Default)]
struct Cave {
    name: String,
    neighbors: Vec<u32>,
    is_small: bool,
}

type CavesMap = Vec<Cave>;

fn is_small_cave(name: &str) -> bool {
    !name.chars().next().unwrap().is_uppercase()
}

fn find_or_create_cave<'a>(caves: &'a mut CavesMap, name: &str) -> (usize, &'a mut Cave) {
    if let Some(cave) = caves.iter().enumerate().find(|(_i, cave)| cave.name == name).map(|c| c.0) {
        return (cave, caves.get_mut(cave).unwrap());
    }

    caves.push(Cave {
        name: String::from(name),
        neighbors: Vec::new(),
        is_small: is_small_cave(name),
    });
    (caves.len() - 1, caves.last_mut().unwrap())
}

fn add_caves_map_link(caves: &mut CavesMap, from: &str, to: &str) {
    if to == "start" || from == "end" {
        return;
    }

    let to = find_or_create_cave(caves, to).0;
    let cave = find_or_create_cave(caves, from).1;

    cave.neighbors.push(to as u32);
}

fn build_map(links: &[InputLink]) -> CavesMap {
    let mut caves  = CavesMap::new();

    caves.push(Cave {
        name: String::from("start"),
        neighbors: Vec::new(),
        is_small: false,
    });
    caves.push(Cave {
        name: String::from("end"),
        neighbors: Vec::new(),
        is_small: false,
    });

    for link in links {
        add_caves_map_link(&mut caves, &link.from, &link.to);
        add_caves_map_link(&mut caves, &link.to, &link.from);
    }

    caves
}

fn count_paths_small_caves_once(caves: &CavesMap, cave: u32, source_path: &[u32]) -> usize {
    let current_cave = caves.get(cave as usize).unwrap();
    if current_cave.neighbors.is_empty() {
        return 1;
    }

    current_cave.neighbors.iter()
        .filter(|neighbor| !caves[**neighbor as usize].is_small || !source_path.contains(*neighbor))
        .map(|neighbor| count_paths_small_caves_once(caves, *neighbor, &[source_path, &[neighbor.clone()]].concat()))
        .sum()
}

fn part1(input: &ParsedInput) -> usize {
    let caves = build_map(&input.links);
    count_paths_small_caves_once(&caves, 0, &Vec::new())
}

struct SourcePath {
    path: Vec<u32>,
    has_twice: usize,
}

impl SourcePath {
    fn new(count: usize) -> Self {
        Self {
            path: Vec::from_iter(std::iter::repeat(u32::MAX).take(count)),
            has_twice: usize::MAX,
        }
    }

    fn can_add(&self, len: usize, cave: u32, is_small: bool) -> bool {
        !is_small || self.has_twice >= len || (self.path[cave as usize] as usize) >= len
    }

    fn with_cave(&mut self, len: usize, cave: u32, is_small: bool) {
        self.path.iter_mut().for_each(|c| if *c as usize == len { *c = u32::MAX; });

        if self.has_twice >= len && is_small && (self.path[cave as usize] as usize) < len {
            self.has_twice = len;
        } else if self.has_twice == len {
            self.has_twice = usize::MAX;
        }

        if self.path[cave as usize] as usize >= len {
            self.path[cave as usize] = len as u32;
        }
    }
}

fn count_paths_small_caves_once_one_twice<'a>(caves: &'a CavesMap, cave: u32, len: usize, source_path: &std::cell::RefCell<SourcePath>) -> usize {
    let current_cave = &caves[cave as usize];
    if current_cave.neighbors.is_empty() {
        return 1;
    }

    current_cave.neighbors.iter()
        .filter_map(|neighbor| {
            let neighbor_cave = &caves[*neighbor as usize];
            if source_path.borrow().can_add(len, *neighbor, neighbor_cave.is_small) {
                Some((neighbor, neighbor_cave))
            } else {
                None
            }
        })
        .map(|neighbor| {
            source_path.borrow_mut().with_cave(len, *neighbor.0, neighbor.1.is_small);
            count_paths_small_caves_once_one_twice(caves, *neighbor.0, len + 1, source_path)
        })
        .sum()
}

fn part2(input: &ParsedInput) -> usize {
    let caves = build_map(&input.links);
    count_paths_small_caves_once_one_twice(&caves, 0, 0, &std::cell::RefCell::new(SourcePath::new(caves.len())))
}

fn main() {
    let test_input = parse_input("test.txt").unwrap();
    let test2_input = parse_input("test2.txt").unwrap();
    let test3_input = parse_input("test3.txt").unwrap();
    assert_eq!(part1(&test_input), 10);
    assert_eq!(part1(&test2_input), 19);
    assert_eq!(part1(&test3_input), 226);
    
    let parsed_input = parse_input("input.txt").unwrap();
    println!("{}", part1(&parsed_input));

    assert_eq!(part2(&test_input), 36);
    assert_eq!(part2(&test2_input), 103);
    assert_eq!(part2(&test3_input), 3509);
    println!("{}", part2(&parsed_input));
}

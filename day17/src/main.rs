use std::{io::{BufReader, BufRead}, fmt::Display};

#[derive(Clone, PartialEq, Eq, Hash)]
struct Point {
    x: i32,
    y: i32,
}

impl Point {
    const fn new(x: i32, y: i32) -> Point {
        Point { x, y }
    }

    const fn translate(&self, offset: &Point) -> Point {
        Point {
            x: self.x + offset.x,
            y: self.y + offset.y,
        }
    }
}

impl Display for Point {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("({}, {})", self.x, self.y))
    }
}

struct Zone {
    from: Point,
    to: Point,
}

struct ParsedInput {
    target_zone: Zone,
}

fn parse_entry<T: AsRef<str>>(line: T) -> Zone {
    let ranges = line.as_ref()
        .split("x=")
        .skip(1)
        .next()
        .unwrap();

    let mut ranges = ranges.split(", y=")
        .map(|range| range.split("..")
            .map(|v| v.parse::<i32>()
                .unwrap()))
        .flatten();

    let from_x = ranges.next().unwrap();
    let to_x = ranges.next().unwrap();
    let from_y = ranges.next().unwrap();
    let to_y = ranges.next().unwrap();

    Zone {
        from: Point::new(from_x, from_y),
        to: Point::new(to_x, to_y),
    }
}

fn parse_input(file: &str) -> std::io::Result<ParsedInput> {
    let file = std::fs::File::open(format!("{}/{}", env!("CARGO_PKG_NAME"), file))?;
    let reader = BufReader::new(file);
    let target_zone = reader.lines()
        .map(Result::unwrap)
        .map(parse_entry)
        .next()
        .unwrap();

    Ok(ParsedInput {
        target_zone,
    })
}

fn find_x_min(zone_x_min: i32) -> i32 {
    let x_min_float: f64 = (((1 + 8 * zone_x_min) as f64).sqrt() - 1.0) / 2.0;
    x_min_float.ceil() as i32
}

fn get_total_dist(v: i32) -> i32 {
    v * (v + 1) / 2
}

fn x_passes_in_zone(x: i32, min: i32, max: i32) -> bool {
    let ending_x = get_total_dist(x);
    if min <= ending_x && ending_x <= max {
        return true;
    }

    let mut d = x;
    let mut cx = x;
    while d <= max && cx > 0 {
        if d >= min {
            return true;
        }

        cx -= 1;
        d += cx;
    }

    false
}

fn y_passes_in_zone(y: i32, min: i32, max: i32) -> bool {
    let mut d = y;
    let mut cy = y;
    while d >= min {
        if d <= max {
            return true;
        }

        cy -= 1;
        d += cy;
    }

    false
}

fn part1(input: &ParsedInput) -> i32 {
    get_total_dist(-input.target_zone.from.y - 1)
}

fn make_possible_y_values(min_y: i32, max_y: i32) -> impl Iterator<Item = i32> {
    (min_y..=(-min_y - 1))
        .filter(move |y| y_passes_in_zone(*y, min_y, max_y))
}

fn next_speed(p: &Point) -> Point {
    Point::new((p.x - 1).max(0), p.y - 1)
}

fn passes_in_zone(speed: &Point, target_zone: &Zone) -> bool {
    let mut speed = speed.clone();
    let mut position = speed.clone();

    while position.x <= target_zone.to.x && position.y >= target_zone.from.y {
        if position.x >= target_zone.from.x && position.y <= target_zone.to.y {
            return true;
        }

        speed = next_speed(&speed);
        position = position.translate(&speed);
    }

    false
}

fn part2(input: &ParsedInput) -> usize {
    let x_min = find_x_min(input.target_zone.from.x);
    let x_max = input.target_zone.to.x;

    (x_min..=x_max)
        .filter(|x| x_passes_in_zone(*x, input.target_zone.from.x, input.target_zone.to.x))
        .map(|x| make_possible_y_values(input.target_zone.from.y, input.target_zone.to.y)
            .map(move |y| Point::new(x, y))
            .filter(|p| passes_in_zone(p, &input.target_zone)))
        .flatten()
        .count()
}

fn main() {
    let test_input = parse_input("test.txt").unwrap();
    assert_eq!(part1(&test_input), 45);
    
    let parsed_input = parse_input("input.txt").unwrap();
    println!("{}", part1(&parsed_input));

    assert_eq!(part2(&test_input), 112);
    println!("{}", part2(&parsed_input));
}

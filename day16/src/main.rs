use std::io::{BufReader, BufRead};


struct ParsedInput {
    entries: Vec<bool>,
}

fn bit_n(digit: u32, bit_index: u32) -> bool {
    ((digit >> bit_index) & 0x01) == 0x01
}

fn parse_entry<T: AsRef<str>>(line: T) -> Vec<bool> {
    line.as_ref()
        .chars()
        .map(|c| c.to_digit(16).unwrap())
        .map(|digit| (0..4).map(move |bit| bit_n(digit, bit)).rev())
        .flatten()
        .collect()
}

fn parse_input(file: &str) -> std::io::Result<ParsedInput> {
    let file = std::fs::File::open(format!("{}/{}", env!("CARGO_PKG_NAME"), file))?;
    let reader = BufReader::new(file);
    let entries = reader.lines()
        .map(Result::unwrap)
        .map(parse_entry)
        .next()
        .unwrap();

    Ok(ParsedInput {
        entries,
    })
}

trait ParsableUnsigned {
    const ZERO: Self;
    const ONE: Self;
}

impl ParsableUnsigned for u8 {
    const ZERO: Self = 0;
    const ONE: Self = 1;
}

impl ParsableUnsigned for u16 {
    const ZERO: Self = 0;
    const ONE: Self = 1;
}

impl ParsableUnsigned for u32 {
    const ZERO: Self = 0;
    const ONE: Self = 1;
}

impl ParsableUnsigned for u64 {
    const ZERO: Self = 0;
    const ONE: Self = 1;
}

fn parse_bits<U, I>(it: I) -> U
    where I: Iterator<Item = bool>, U: ParsableUnsigned + std::ops::BitOr<Output = U> + std::ops::Shl<Output = U> {
    it.fold(U::ZERO, |v, b| {
        (v << U::ONE) | if b { U::ONE } else { U::ZERO }
    })
}

enum LengthType {
    Bits,
    Packets,
}

enum Operator {
    Sum,
    Product,
    Minimum,
    Maximum,
    GreaterThan,
    LessThan,
    Equals,
}

impl Operator {
    fn from(type_id: u8) -> Self {
        match type_id {
            0 => Self::Sum,
            1 => Self::Product,
            2 => Self::Minimum,
            3 => Self::Maximum,
            5 => Self::GreaterThan,
            6 => Self::LessThan,
            7 => Self::Equals,
            _ => unreachable!(),
        }
    }

    fn execute<I: Iterator<Item = u64>>(&self, mut values: I) -> u64 {
        match self {
            Operator::Sum => values.sum(),
            Operator::Product => values.product(),
            Operator::Minimum => values.min().unwrap(),
            Operator::Maximum => values.max().unwrap(),
            Operator::GreaterThan => if values.next().unwrap() > values.next().unwrap() { 1 } else { 0 },
            Operator::LessThan => if values.next().unwrap() < values.next().unwrap() { 1 } else { 0 },
            Operator::Equals => if values.next().unwrap() == values.next().unwrap() { 1 } else { 0 },
        }
    }
}

enum PacketData {
    Litteral(u64),
    Operator(Operator, Vec<Packet>),
}

impl PacketData {
    fn from(type_id: u8, it: &mut dyn Iterator<Item = bool>) -> Self {
        match type_id {
            0x04 => {
                let mut v = 0u64;
                loop {
                    let is_last = !it.next().unwrap();

                    v = (v << 4) | parse_bits::<u64, _>(it.take(4));

                    if is_last {
                        break;
                    }
                }
                Self::Litteral(v)
            },
            _ => {
                let length_type = if it.next().unwrap() { LengthType::Packets } else { LengthType::Bits };
                let mut packets = Vec::new();

                match length_type {
                    LengthType::Bits => {
                        let length: u16 = parse_bits(it.take(15));
                        let sub_it = &mut it.take(length as usize).peekable();
                        while let Some(_) = sub_it.peek() {
                            packets.push(Packet::from(sub_it));
                        }
                    },
                    LengthType::Packets => {
                        let length: u16 = parse_bits(it.take(11));
                        for _ in 0..length {
                            packets.push(Packet::from(it));
                        }
                    },
                };

                Self::Operator(Operator::from(type_id), packets)
            },
        }
    }

    fn versions_sum(&self) -> usize {
        match self {
            Self::Litteral(_) => 0,
            Self::Operator(_op, packets) =>
                packets.iter()
                    .map(|p| p.versions_sum())
                    .sum(),
        }
    }

    fn compute_value(&self) -> u64 {
        match self {
            Self::Litteral(v) => *v,
            Self::Operator(op, packets) =>
                op.execute(packets.iter().map(|p| p.compute_value()))
        }
    }
}

struct Packet {
    version: u8,
    data: PacketData,
}

impl Packet {
    fn from(it: &mut dyn Iterator<Item = bool>) -> Self {
        let version = parse_bits(it.take(3));
        let type_id = parse_bits(it.take(3));
        Self {
            version,
            data: PacketData::from(type_id, it),
        }
    }

    fn versions_sum(&self) -> usize {
        self.version as usize + self.data.versions_sum()
    }

    fn compute_value(&self) -> u64 {
        self.data.compute_value()
    }
}

fn part1(input: &ParsedInput) -> usize {
    let root = Packet::from(&mut input.entries.iter().cloned());
    root.versions_sum()
}

fn part2(input: &ParsedInput) -> usize {
    let root = Packet::from(&mut input.entries.iter().cloned());
    root.compute_value() as usize
}

fn main() {
    let test_input = parse_input("test.txt").unwrap();
    let test_input2 = parse_input("test2.txt").unwrap();
    let test_input3 = parse_input("test3.txt").unwrap();
    let test_input4 = parse_input("test4.txt").unwrap();
    assert_eq!(part1(&test_input), 16);
    assert_eq!(part1(&test_input2), 12);
    assert_eq!(part1(&test_input3), 23);
    assert_eq!(part1(&test_input4), 31);
    
    let parsed_input = parse_input("input.txt").unwrap();
    println!("{}", part1(&parsed_input));

    let test_input5 = parse_input("test5.txt").unwrap();
    let test_input6 = parse_input("test6.txt").unwrap();
    let test_input7 = parse_input("test7.txt").unwrap();
    let test_input8 = parse_input("test8.txt").unwrap();
    let test_input9 = parse_input("test9.txt").unwrap();
    let test_input10 = parse_input("test10.txt").unwrap();
    let test_input11 = parse_input("test11.txt").unwrap();
    let test_input12 = parse_input("test12.txt").unwrap();
    assert_eq!(part2(&test_input5), 3);
    assert_eq!(part2(&test_input6), 54);
    assert_eq!(part2(&test_input7), 7);
    assert_eq!(part2(&test_input8), 9);
    assert_eq!(part2(&test_input9), 1);
    assert_eq!(part2(&test_input10), 0);
    assert_eq!(part2(&test_input11), 0);
    assert_eq!(part2(&test_input12), 1);
    println!("{}", part2(&parsed_input));
}
